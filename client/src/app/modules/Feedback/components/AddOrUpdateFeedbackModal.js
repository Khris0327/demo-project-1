import React, { Suspense } from 'react';
import { Form, Modal, Space, Button, Popconfirm } from 'antd';
import { DeleteFilled } from '@ant-design/icons';

import { inject, observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactText, FallBackLoaders } from '@core_common/components';

import { FeedbackController } from '@app_modules/Feedback/controller';
import {
  CallerTypeForm,
  FeedbackForm,
  //DateForm,
  FailureDropdownsForm,
  AdditionalDetailsForm
} from '@app_modules/Feedback/components';

import CountryData from '@core_data/countries/Countries.json';

function AddOrUpdateFeedbackModal({ store, form }) {
  const { t } = useTranslation('common');

  const {
    handleToggleAddOrUpdateShowFeedbackModal,
    handleChangeForm,
    handleUpdateOrCreateFeedback,
    handleDeleteFeedback
  } = FeedbackController({
    store,
    CountryData,
    form,
    t
  });

  const { isUpdate, showAddOrUpdateFeedbackModal } = store.FeedbackManagementUtilities;
  const { isCreating } = store.feedback;
  const { EmptyLoader } = FallBackLoaders;

  return (
    <Modal
      title={
        <ExactText text={isUpdate ? t('Feedback_#Update Feedback') : t('Feedback_#New Feedback')} />
      }
      visible={showAddOrUpdateFeedbackModal}
      footer={null}
      className="exact-modal"
      style={{ top: 20 }}
      onCancel={() => handleToggleAddOrUpdateShowFeedbackModal()}
    >
      <Form
        form={form}
        name="control-hooks"
        layout="vertical"
        onFieldsChange={handleChangeForm}
        onFinish={(values) => handleUpdateOrCreateFeedback(values, isUpdate)}
      >
        {isUpdate && (
          <div className="d-flex w-100 justify-content-end">
            <Popconfirm
              cancelText={t('Feedback_#Cancel')}
              okText={t('Feedback_#Ok')}
              title={t('Feedback_#Möchten Sie den Eintrag wirklich löschen?')}
              onConfirm={handleDeleteFeedback}
            >
              <Button danger type="text">
                <DeleteFilled />
              </Button>
            </Popconfirm>
          </div>
        )}

        <div id="profile">
          <Suspense fallback={EmptyLoader}>
            <FeedbackForm />
          </Suspense>
          <Suspense fallback={EmptyLoader}>
            <CallerTypeForm />
          </Suspense>
          {/* <Suspense fallback={EmptyLoader}>
            <DateForm />
          </Suspense> */}
          <Suspense fallback={EmptyLoader}>
            <FailureDropdownsForm />
          </Suspense>
          <Suspense fallback={EmptyLoader}>
            <AdditionalDetailsForm />
          </Suspense>
        </div>

        <Form.Item>
          <div className="d-flex w-100 justify-content-end">
            <Space>
              <Button onClick={() => handleToggleAddOrUpdateShowFeedbackModal()} type="default">
                {t('Feedback_#Cancel')}
              </Button>
              <Button loading={isCreating} type="primary" htmlType="submit">
                {t(isUpdate ? t('Feedback_#Update') : t('Feedback_#Submit'))}
              </Button>
            </Space>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default inject('store')(observer(AddOrUpdateFeedbackModal));
