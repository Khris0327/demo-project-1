import { TWO_GRID } from '@core_common/antdhelpers/constants';
import { ExactText } from '@core_common/components';
import { Col, Row, Form, Input, Select } from 'antd';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { required } from '@core_common/antdhelpers/helperfunctions';

const { Option } = Select;

function AdditionalDetailsForm({ store }) {
  const { t } = useTranslation('common');

  return (
    <div>
      <div className="mb-2">
        <ExactText text={t('Feedback_#Additional Details')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            name="affected_employee"
            label={t('Feedback_#Affected Employee')}
            //rules={[required(t(`Please enter Affected Employee`))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Affected Employee')}
            />
          </Form.Item>
        </Col>
        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="internal_measures"
            label={t('Feedback_#Internal Measures')}
            //rules={[required(t('Feedback_#Please enter Internal Measures'))]}
          >
            <Select
              listItemHeight={10}
              listHeight={250}
              showSearch={true}
              placeholder={t('Feedback_#Internal Measures')}
              className="w-100 "
              dropdownClassName="myclassname"
              autoComplete="newpassword"
            >
              {store.feedback.singleState.internal_measures_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col> */}
        <Col {...TWO_GRID}>
          <Form.Item
            name="form_of_execution"
            label={t('Feedback_#Form of Execution')}
            // rules={[required(t('Feedback_#Please enter Form of Execution'))]}
          >
            <Select
              showSearch={true}
              placeholder={t('Feedback_#Form of Execution')}
              className="w-100"
              autoComplete="newpassword"
            >
              {store.feedback.singleState.form_of_execution_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="handling_by"
            label={t('Feedback_#Handling by')}
            autoComplete="newpassword"
            //rules={[required(t('Feedback_#Please enter Handling by'))]}
          >
            <Select showSearch={true} placeholder={t('Feedback_#Handling by')} className="w-100">
              {store.feedback.userDropdownState.map((dropdown, index) => (
                <Option key={index} value={dropdown.id}>
                  {dropdown.first_name} {dropdown.last_name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="registered_by"
            label={t('Feedback_#Registered by')}
            //rules={[required(t('Feedback_#Please enter Registered by'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Registered by')}
              className="w-100"
            >
              {store.feedback.userDropdownState.map((dropdown, index) => (
                <Option key={index} value={dropdown.id}>
                  {dropdown.first_name} {dropdown.last_name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col> */}
        <Col {...TWO_GRID}>
          <Form.Item
            name="corrective_actions"
            label={t('Feedback_#Corrective Actions')}
            //rules={[required(t(`Please enter Affected Employee`))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Corrective Actions')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="status"
            label={t('Feedback_#Status')}
            rules={[required(t('Feedback_#Please enter Status'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Status')}
              className="w-100"
            >
              {store.feedback.singleState.status_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
}

export default inject('store')(observer(AdditionalDetailsForm));
