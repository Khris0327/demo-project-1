import { TWO_GRID } from '@core_common/antdhelpers/constants';
import { email, required } from '@core_common/antdhelpers/helperfunctions';
import { ExactText } from '@core_common/components';
import { Col, Row, Form, Input, InputNumber } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

function CallerTypeForm() {
  const { t } = useTranslation('common');

  return (
    <div>
      <div className="mb-2">
        <ExactText text={t('Feedback_#Reporter Name')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            name="order_number"
            label={t('Feedback_#Order Number')}
            rules={[required(t('Feedback_#Please enter Order Number'))]}
          >
            <Input
              autoComplete="newpassword"
              Number
              className="w-100"
              placeholder={t('Feedback_#Order Number')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="company_name"
            label={t('Feedback_#Company Name')}
            rules={[required(t('Feedback_#Please enter Company Name'))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Company Name')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="last_name"
            label={t('Feedback_#Prename / Last Name')}
            rules={[required(t('Feedback_#Please enter Prename / Last Name'))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Prename / Last Name')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="position"
            label={t('Feedback_#Position')}
            rules={[required(t('Feedback_#Please enter Position'))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Position')}
            />
          </Form.Item>
        </Col>

        <Col {...TWO_GRID}>
          <Form.Item name="street_no" label={t('Feedback_#Street + Nr.')}>
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Street + Nr.')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item name="zip_code" label={t('Feedback_#ZIP')}>
            <InputNumber
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#ZIP')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item name="city" label={t('Feedback_#City')}>
            <Input autoComplete="newpassword" className="w-100" placeholder={t('Feedback_#City')} />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item name="phone_number" label={t('Feedback_#Phone Number')}>
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Phone Number')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="email"
            label={t('Feedback_#Email Address')}
            rules={[email(t('Feedback_#Please enter email address'))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Email Address')}
            />
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
}

export default CallerTypeForm;
