import { TWO_GRID } from '@core_common/antdhelpers/constants';
import { ExactText } from '@core_common/components';
import { Col, Row, Form, DatePicker } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import InputMask from 'react-input-mask';
import { inject, observer } from 'mobx-react';

//APP IMPORTS
import { FeedbackController } from '@app_modules/Feedback/controller';

function DateForm({ store }) {
  const { t } = useTranslation('common');

  const { handleGetDateFormat } = FeedbackController({ store });

  return (
    <div>
      <div className="mb-2">
        <ExactText text={t('Feedback_#Event Date_Time')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            name="event_date"
            label={t('Feedback_#Date')}
            // rules={[required(t('Feedback_#Please enter Event Date'))]}
          >
            <DatePicker
              placeholder={t('Feedback_#Date')}
              format={handleGetDateFormat()}
              className="w-100"
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="event_time"
            label={t('Feedback_#Time')}
            // rules={[required(t('Feedback_#Please enter Event Time'))]}
          >
            <InputMask placeholder="HH:mm" className="ant-input w-100" mask={'99:99'} />
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
}

export default inject('store')(observer(DateForm));
