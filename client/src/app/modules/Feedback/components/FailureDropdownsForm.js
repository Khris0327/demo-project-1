import { TWO_GRID, FULL_GRID } from '@core_common/antdhelpers/constants';
//import { required } from '@core_common/antdhelpers/helperfunctions';
import { ExactText } from '@core_common/components';
import { Col, Row, Form, Input, Select } from 'antd';
import { inject, observer } from 'mobx-react';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { FeedbackController } from '../controller';

const { Option } = Select;

function FailureDropdownsForm({ store }) {
  const { t } = useTranslation('common');

  const { hasDropdown } = FeedbackController({ store, t });

  return (
    <div>
      <div className="mb-2">
        <ExactText text={t('Feedback_#Failure Class')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            name="failure_class_parent"
            label={t('Feedback_#Failure Class')}
            // rules={[required(t('Feedback_#Please enter Failure Class'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Failure Class')}
              className="w-100"
            >
              {store.feedback.singleState.failure_class_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="failure_class"
            label={t(store.feedback.singleState.currentSelectedDropdown)}
            // rules={[
            //   required(t(`Please enter ${store.feedback.singleState.currentSelectedDropdown}`))
            // ]}
          >
            {hasDropdown(store.feedback.singleState.currentSelectedDropdown) ? (
              <Select
                autoComplete="newpassword"
                showSearch={true}
                placeholder={t(store.feedback.singleState.currentSelectedDropdown)}
                className="w-100"
              >
                {store.feedback.singleState.child_dropdown_list.map((dropdown, index) => (
                  <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                    {dropdown[`entry_${store.globalState.language}`]}
                  </Option>
                ))}
              </Select>
            ) : (
              <Input
                autoComplete="newpassword"
                className="w-100"
                placeholder={t(store.feedback.singleState.currentSelectedDropdown)}
              />
            )}
          </Form.Item>
        </Col>
        <Col {...FULL_GRID}>
          <Form.Item
            name="event_details"
            label={t('Feedback_#Event Details')}
            //rules={[required(t(`Please enter ${'Event Details'}`))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Event Details')}
            />
          </Form.Item>
        </Col>
      </Row>
    </div>
  );
}

export default inject('store')(observer(FailureDropdownsForm));
