import React, { useEffect } from 'react';
import { Col, DatePicker, Form, Row, Select } from 'antd';
import { useTranslation } from 'react-i18next';

/**APP IMPORTS */
import { FeedbackController } from '@app_modules/Feedback/controller';

/**CORE IMPORTS */
import { required } from '@core_common/antdhelpers/helperfunctions';
import { TWO_GRID } from '@core_common/antdhelpers/constants';
import { inject, observer } from 'mobx-react';

const { Option } = Select;

function FeedbackForm({ store }) {
  const { t } = useTranslation('common');

  const { getDropdown, handleGetUsersDropdown, handleGetDateFormat } = FeedbackController({
    store
  });

  // eslint-disable-next-line
  useEffect(() => {
    getDropdown('feedback_type');
    getDropdown('reporter_category');
    getDropdown('status');
    getDropdown('failure_class');
    // getDropdown('internal_measures');
    getDropdown('form_of_execution');
    handleGetUsersDropdown();
  }, []);

  return (
    <>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            className="w-100"
            name="feedback_date"
            label={t('Feedback_#Feedback Date')}
            rules={[required(t('Feedback_#Please enter Feedback Date'))]}
          >
            <DatePicker
              placeholder={t('Feedback_#Feedback Date')}
              format={handleGetDateFormat()}
              className="w-100"
            />
          </Form.Item>
        </Col>

        <Col {...TWO_GRID}>
          <Form.Item
            name="feedback_type"
            label={t('Feedback_#Feedback Type')}
            rules={[required(t('Feedback_#Please enter Feedback Type'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Feedback Type')}
              className="w-100"
            >
              {store.feedback.singleState.feedback_type_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="order_number"
            label={t('Feedback_#Order Number')}
            rules={[required(t('Feedback_#Please enter Order Number'))]}
          >
            <Input
              autoComplete="newpassword"
              Number
              className="w-100"
              placeholder={t('Feedback_#Order Number')}
            />
          </Form.Item>
        </Col> */}
        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="reporter_category"
            label={t('Feedback_#Reporter Category')}
            //rules={[required(t('Feedback_#Please enter Reporter Category'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Reporter Category')}
              className="w-100"
            >
              {store.feedback.singleState.reporter_category_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col> */}

        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="status"
            label={t('Feedback_#Status')}
            rules={[required(t('Feedback_#Please enter Status'))]}
          >
            <Select
              autoComplete="newpassword"
              showSearch={true}
              placeholder={t('Feedback_#Status')}
              className="w-100"
            >
              {store.feedback.singleState.status_list.map((dropdown, index) => (
                <Option key={index} value={dropdown[`entry_${store.globalState.language}`]}>
                  {dropdown[`entry_${store.globalState.language}`]}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col> */}

        {/* <Col {...TWO_GRID}>
          <Form.Item
            name="event_place"
            label={t('Feedback_#Event Place')}
            //rules={[required(t('Feedback_#Please enter Event Place'))]}
          >
            <Input
              autoComplete="newpassword"
              className="w-100"
              placeholder={t('Feedback_#Event Place')}
            />
          </Form.Item>
        </Col> */}
      </Row>
    </>
  );
}

export default inject('store')(observer(FeedbackForm));
