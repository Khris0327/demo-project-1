import React, { Suspense } from 'react';
import { Button, Col, Input, Popconfirm, Row, Space, Tooltip } from 'antd';
import { inject, observer } from 'mobx-react';
import { PlusOutlined, DeleteOutlined, DownloadOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactTitle, FallBackLoaders } from '@core_common/components';
import { AddOrUpdateFeedbackModal } from '@app_modules/Feedback/components';
import { FeedbackController } from '@app_modules/Feedback/controller';
import { ANTD_HALF_COL } from '@core_common/antdhelpers/constants';

function FeedbackHeader({ isDeleteVisible, setIsDeleteVisible, store, form }) {
  const { t } = useTranslation('common');

  const {
    handleToggleAddOrUpdateShowFeedbackModal,
    handleFeedbackSearch,
    handleDeleteMultiple,
    handleExportData
  } = FeedbackController({ form, store, setIsDeleteVisible, t });

  return (
    <>
      <Row>
        <Col md={{ span: ANTD_HALF_COL }}>
          {' '}
          <ExactTitle level={3} text={t('Feedback_#Feedback')} />
        </Col>
        <Col
          className="d-flex w-100 justify-content-end align-items-center"
          md={{ span: ANTD_HALF_COL }}
        >
          {' '}
          <Space className="mb-3">
            {isDeleteVisible && (
              <Popconfirm
                onConfirm={handleDeleteMultiple}
                title={t('Feedback_#Are you sure?')}
                okText={t('Feedback_#Yes')}
                cancelText={t('Feedback_#No')}
              >
                <Button className="shadow-sm" type="primary" danger icon={<DeleteOutlined />} />
              </Popconfirm>
            )}

            <Button
              onClick={() => handleToggleAddOrUpdateShowFeedbackModal()}
              className="shadow-sm"
              type="default"
              icon={<PlusOutlined />}
            />

            <Tooltip title={t('Feedback_#EXPORT DATA TO CSV')}>
              <Button
                onClick={handleExportData}
                className="shadow-sm"
                loading={store.FeedbackManagementUtilities.isDownloading}
                type="default"
                icon={<DownloadOutlined />}
              />
            </Tooltip>

            <Input.Search
              className="shadow-sm"
              onSearch={handleFeedbackSearch}
              id="exact-search"
              placeholder={t('Feedback_#Search')}
              enterButton={t('Feedback_#Search')}
            />
          </Space>
        </Col>
      </Row>

      <Suspense fallback={FallBackLoaders.EmptyLoader}>
        <AddOrUpdateFeedbackModal form={form} />
      </Suspense>
    </>
  );
}

export default inject('store')(observer(FeedbackHeader));
