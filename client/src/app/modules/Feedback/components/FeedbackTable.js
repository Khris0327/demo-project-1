import React, { useEffect } from 'react';

import { Button, Divider, Space, Table, Typography } from 'antd';
import { EditFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import 'dayjs/locale/de';

/**CORE IMPORTS */
import { FeedbackController } from '@app_modules/Feedback/controller';
import dayjs from 'dayjs';

const { Text } = Typography;

function FeedbackTable({ store, form, setIsDeleteVisible }) {
  const { t } = useTranslation('common');

  const {
    getFeedbacks,
    onChangePage,
    handleToggleAddOrUpdateShowFeedbackModal,
    handleSetSelectedRowKeys
  } = FeedbackController({ store, form, t });

  // eslint-disable-next-line
  useEffect(getFeedbacks, []);

  const columns = [
    {
      title: t('Feedback_#Feedback Data'),
      render: (FeedbackData) => (
        <Space
          split={<Divider style={{ paddingTop: 10, paddingBottom: 10, margin: 0 }} />}
          direction="vertical"
        >
          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Reporter Name')}</Text>
            <Text strong>
              {FeedbackData.company_name && `${FeedbackData.company_name} - `}
              {FeedbackData.last_name}
            </Text>
          </Space>

          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Feedback Date')}</Text>
            <Text strong>
              {dayjs(FeedbackData.feedback_date)
                .locale(store.globalState.language)
                .format('DD. MMMM YYYY')}
            </Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Order Number')}</Text>
            <Text strong>{FeedbackData.order_number}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Feedback Type')}</Text>
            <Text strong>{FeedbackData.feedback_type}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Handling by')}</Text>
            <Text strong>
              {FeedbackData.UserHandlingBy
                ? `${FeedbackData.UserHandlingBy.first_name} ${FeedbackData.UserHandlingBy.last_name}`
                : ''}
            </Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Feedback_#Status')}</Text>
            <Text strong>{FeedbackData.status}</Text>
          </Space>

          <Space>
            <Button
              onClick={() => handleToggleAddOrUpdateShowFeedbackModal(true, FeedbackData)}
              icon={<EditFilled />}
              type="link"
            >
              {t('Feedback_#Update')}
            </Button>
          </Space>
        </Space>
      ),
      responsive: ['xs']
    },

    {
      title: t('Feedback_#Reporter Name').toUpperCase(),
      dataIndex: 'last_name',
      render: (text, FeedbackData) => (
        <>
          {FeedbackData.company_name && `${FeedbackData.company_name} - `}
          {text}
        </>
      ),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      sorter: (a, b) => {
        if (a.company_name && b.company_name) {
          return a.company_name.toLowerCase().localeCompare(b.company_name.toLowerCase());
        } else {
          return a.last_name.toLowerCase().localeCompare(b.last_name.toLowerCase());
        }
      }, // a.reporter_name.toLowerCase().localeCompare(b.reporter_name.toLowerCase()),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Feedback_#Feedback Date').toUpperCase(),
      dataIndex: 'feedback_date',
      sorter: (a, b) => a.feedback_date.toLowerCase().localeCompare(b.feedback_date.toLowerCase()),
      responsive: ['sm', 'md', 'lg', 'xl'],
      render: (data, FeedbackData) => {
        return <>{dayjs(data).locale(store.globalState.language).format('DD. MMMM YYYY')}</>;
      }
    },
    {
      title: t('Feedback_#Order Number').toUpperCase(),
      dataIndex: 'order_number',
      sorter: (a, b) => a.order_number.toLowerCase().localeCompare(b.order_number.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Feedback_#Feedback Type').toUpperCase(),
      dataIndex: 'feedback_type',

      sorter: (a, b) => a.feedback_type.toLowerCase().localeCompare(b.feedback_type.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Feedback_#Handling by').toUpperCase(),
      dataIndex: 'handling_by',

      sorter: (a, b) => a.handling_by?.toLowerCase().localeCompare(b.handling_by?.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl'],
      render: (_, FeedbackData) => {
        return (
          <>
            {FeedbackData.UserHandlingBy
              ? `${FeedbackData.UserHandlingBy.first_name} ${FeedbackData.UserHandlingBy.last_name}`
              : ''}
          </>
        );
      }
    },
    {
      title: t('Feedback_#Status').toUpperCase(),
      dataIndex: 'status',

      sorter: (a, b) => a.status.toLowerCase().localeCompare(b.status.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },

    {
      title: t('Feedback_#Action').toUpperCase(),
      dataIndex: 'action',
      responsive: ['lg', 'xl'],
      shouldCellUpdate: (prev, next) => false,
      render: (_, FeedbackData) => {
        return (
          <>
            <Button
              onClick={() => {
                handleToggleAddOrUpdateShowFeedbackModal(true, FeedbackData);
              }}
              icon={<EditFilled />}
              type="link"
            />
          </>
        );
      }
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      let hasChecked = selectedRows.length > 0;
      //console.log(selectedRowKeys, selectedRows[0]?.toJSON());

      setIsDeleteVisible(hasChecked);
      handleSetSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      firstName: record.name
    })
  };

  return (
    <Table
      loading={store.feedback.loading}
      className="exact-table shadow-sm bg-white p-3"
      size="small"
      locale={{
        triggerDesc: t('Feedback_#Click to sort descending'),
        triggerAsc: t('Feedback_#Click to sort ascending'),
        cancelSort: t('Feedback_#Click to cancel sorting')
      }}
      rowSelection={{
        type: 'checkbox',
        ...rowSelection
      }}
      columns={columns}
      dataSource={store.feedback.state.toJSON()}
      pagination={{
        position: ['bottomCenter'],
        hideOnSinglePage: true,
        pageSize: 10,
        responsive: true,
        onChange: onChangePage,
        showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} Feedbacks`,
        // pageSizeOptions: [5, 10, 15, 20],
        showSizeChanger: false,
        total: store.feedback.toJSON().total
      }}
    />
  );
}

export default inject('store')(observer(FeedbackTable));
