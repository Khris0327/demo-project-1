import React from 'react';
import InputMask from 'react-input-mask';
import { Input, Form } from 'antd';

const MaskInput = (props) => {
  const { disabled, mask, label, meta, required } = props;

  //console.log(mask, disabled);
  return (
    <Form.Item
      label={'TEST'}
      //  validateStatus={meta.touched ? (meta.error ? 'error' : 'success') : undefined}
      // help={meta.touched ? (meta.error ? meta.error : undefined) : undefined}
      //hasFeedback={meta.touched ? true : false}
      //required={required}
    >
      <InputMask className="ant-input w-100" mask={mask} />

      {/* {(inputProps) => <Input {...inputProps} type="tel" />} */}
      {/* </InputMask> */}
    </Form.Item>
  );
};

export default MaskInput;
