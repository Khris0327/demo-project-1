import { message } from 'antd';
import weekday from 'dayjs/plugin/weekday';
import localeData from 'dayjs/plugin/localeData';
import dayjs from 'dayjs';
dayjs.extend(weekday);
dayjs.extend(localeData);

const FeedbackController = ({ store, form, t, setIsDeleteVisible }) => {
  const getFeedbacks = async () => {
    const page = 1;

    const search = store.FeedbackManagementUtilities.search;
    const props = store.FeedbackManagementUtilities.props;
    let params = { page, search, props };

    await store.feedback.LIST(params);
  };

  const handleGetUsersDropdown = async () => {
    await store.feedback.getUsersDropdown();
  };

  const onChangePage = async (page) => {
    const search = store.FeedbackManagementUtilities.search;
    const props = store.FeedbackManagementUtilities.props;

    let params = { page, search, props, user_type: store.login.type, user_id: store.login.id };

    await store.feedback.LIST(params);
  };

  const handleDeleteMultiple = async () => {
    const selectedRowIds = store.FeedbackManagementUtilities.selectedRowsKeys;
    const currentPage = store.FeedbackManagementUtilities.currentPage;
    //('SelectedKey', selectedRowIds.toJSON());
    await store.feedback.DELETE(selectedRowIds);
    await onChangePage(currentPage);
    setIsDeleteVisible(false);
  };

  const handleToggleAddOrUpdateShowFeedbackModal = (isUpdate = false, FeedbackData = null) => {
    form.resetFields();
    if (isUpdate) {
      FeedbackData = store.feedback.state.find((c) => c.id === FeedbackData?.id);

      handleUpdateChildDropdown(FeedbackData.failure_class_parent);
      form.setFieldsValue({
        ...FeedbackData,
        feedback_date: FeedbackData.feedback_date ? dayjs(FeedbackData.feedback_date) : '',
        event_date: FeedbackData.event_date ? dayjs(FeedbackData.event_date) : ''
        // event_time: dayjs(FeedbackData.event_time)
      });

      store.FeedbackManagementUtilities.setUpdateId(FeedbackData?.id);
    }
    store.FeedbackManagementUtilities.setToggleShowAddOrUpdateFeedbackModal(isUpdate);
  };

  const deleteFile = async () => {
    store.feedback.deleteFile();
  };

  const handleExportData = async () => {
    store.FeedbackManagementUtilities.setIsDownloading(true);

    let response = await store.feedback.exportData();

    if (response.data) {
      //let response = await store.feedback.getCSV();

      //const url = window.URL.createObjectURL(new Blob([response.data]));

      response.data = _changeCSVSeparator(response.data);

      const url = window.URL.createObjectURL(
        new Blob(['\ufeff', response.data], {
          type: response.headers['content-type'] + ';text/csv;charset=utf-8'
        })
      );
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `export_file.csv`); //or any other extension
      document.body.appendChild(link);
      link.click();
      await deleteFile();

      store.FeedbackManagementUtilities.setIsDownloading(false);
    } else {
      message.warning(t('Export_#No data to export'));

      store.FeedbackManagementUtilities.setIsDownloading(false);
    }
  };

  const handleChangeForm = async (values) => {
    if (values[0].name[0] === 'failure_class_parent') {
      let val = values[0].value;

      form.setFieldsValue({ failure_class: '' });
      await handleUpdateChildDropdown(val);
      // store.feedback.singleState.setCurrentSelectedDropdown(val);
    }
  };
  const handleUpdateChildDropdown = async (value) => {
    await getChildDropdown(value);
    store.feedback.singleState.setCurrentSelectedDropdown(value);
  };
  const getDropdown = async (type) => {
    let { data } = await store.dropdownSettings.getDropdownSetting(type);

    store.feedback.singleState.setDropdownList(type, data.data);
  };

  const getChildDropdown = async (type) => {
    let { data } = await store.dropdownSettings.getDropdownSetting(type);

    store.feedback.singleState.setDropdownList('child_dropdown', data.data);
  };

  const addUserId = (values) => {
    values.user_id = store.login.id;
    return values;
  };

  const handleUpdateOrCreateFeedback = async (values, isUpdate, isSelf = false) => {
    //return console.log(values, 'Test');
    values = addUserId(values);
    //console.log('feedback', values);
    message.loading({
      content: isUpdate ? t('Feedback_#Updating Feedback') : t('Feedback_#Creating Feedback'),
      key: 'creatingFeedback'
    });

    const params = isUpdate
      ? [store.FeedbackManagementUtilities.updateId, values] // If updating self use id in login store
      : [values];

    let [successMessage, error] = await store.feedback[isUpdate ? 'UPDATE' : 'CREATE'](...params);

    let success = await _showresultMessage(error, {
      successMessage: t(successMessage?.message) // isUpdate ? t('Feedback_#Feedback Updated') :
    });

    if (success) {
      if (!isSelf) {
        form.resetFields();
        store.FeedbackManagementUtilities.setToggleShowAddOrUpdateFeedbackModal();
      }
    }
  };

  const handleFetchFeedbackTypes = async () => {
    await store.FeedbackManagementUtilities.FETCH_Feedback_TYPES();
  };

  const handleFeedbackSearch = async (search) => {
    search = search.trim();
    const props = store.FeedbackManagementUtilities.props;
    let params = { search, props, user_type: store.login.type, user_id: store.login.id };

    store.FeedbackManagementUtilities.setSearch(search);
    await store.feedback.LIST(params);
  };

  const handleChangeFeedbackStatus = async (params) => {
    let { FeedbackData, active } = params;

    let [successMessage, error] = await store.feedback.UPDATE(FeedbackData.id, {
      active
    });

    await _showresultMessage(error, {
      successMessage: successMessage.message
    });
  };

  const handleSendEmailToFeedback = async (FeedbackData) => {
    message.loading({
      content: t('Feedback_#Sending Email'),
      key: 'creatingFeedback'
    });

    let [successResponse, error] = await store.feedback.SEND_EMAIL({
      id: FeedbackData.id,
      email: FeedbackData.email
    });

    await _showresultMessage(error, {
      successMessage: t(successResponse?.message)
    });
  };

  const hasDropdown = (selected) => {
    if (selected === 'FK8 Fehler nicht zuordbar' || selected === 'Lob') {
      return false;
    }

    return true;
  };

  const handleDeleteFeedback = async () => {
    let data = form.getFieldValue();
    await store.feedback.DELETE(data.id);
    getFeedbacks();

    store.FeedbackManagementUtilities.setToggleShowAddOrUpdateFeedbackModal();
    form.resetFields();
  };

  const handleSetSelectedRowKeys = async (setSelectedRowsKeys) => {
    store.FeedbackManagementUtilities.setSelectedRowsKeys(setSelectedRowsKeys);
  };

  const handleGetDateFormat = () => {
    return store.FeedbackManagementUtilities.getDateFormat(store.globalState.language);
  };

  async function _showresultMessage(error, { successMessage }) {
    return new Promise((resolve, reject) => {
      if (error) {
        message.error({
          content: t(error.response.data.error.message),
          key: 'creatingFeedback'
        });

        reject(false);
      } else {
        message.success({ content: successMessage, key: 'creatingFeedback' });
        resolve(true);
      }
    });
  }

  function _changeCSVSeparator(data) {
    let locale = store.globalState.language;

    switch (locale) {
      case 'de':
        return data.replace(/,/g, ';');
      default:
        return data;
    }
  }

  return {
    getFeedbacks,
    onChangePage,
    handleToggleAddOrUpdateShowFeedbackModal,
    handleChangeForm,
    handleUpdateOrCreateFeedback,
    handleFetchFeedbackTypes,
    handleFeedbackSearch,
    handleChangeFeedbackStatus,
    handleSendEmailToFeedback,
    handleDeleteFeedback,
    handleDeleteMultiple,
    handleSetSelectedRowKeys,
    getDropdown,
    hasDropdown,
    handleGetUsersDropdown,
    handleExportData,
    handleGetDateFormat
  };
};

export default FeedbackController;
