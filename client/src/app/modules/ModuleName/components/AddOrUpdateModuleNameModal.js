import React, { Suspense } from 'react';
import { Form, Modal, Space, Button, Checkbox, Popconfirm } from 'antd';
import { DeleteFilled } from '@ant-design/icons';

import { inject, observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactText, FallBackLoaders } from '@core_common/components';

import { ModuleNameController } from '@app_modules/ModuleName/controller';
import { ModuleNameForm } from '@app_modules/ModuleName/components';

import CountryData from '@core_data/countries/Countries.json';

function AddOrUpdateModuleNameModal({ store, form }) {
  const { t } = useTranslation('common');

  const {
    handleToggleAddOrUpdateShowModuleNameModal,
    handleChangeForm,
    handleUpdateOrCreateModuleName,
    handleDeleteModuleName
  } = ModuleNameController({
    store,
    CountryData,
    form,
    t
  });

  const { isUpdate, showAddOrUpdateModuleNameModal } = store.ModuleNameManagementUtilities;
  const { isCreating } = store.moduleName;
  const { EmptyLoader } = FallBackLoaders;

  return (
    <Modal
      title={<ExactText text={isUpdate ? t('Update ModuleName') : t('Neue ModuleName')} />}
      visible={showAddOrUpdateModuleNameModal}
      footer={null}
      className="exact-modal"
      style={{ top: 20 }}
      onCancel={() => handleToggleAddOrUpdateShowModuleNameModal()}
    >
      <Form
        form={form}
        name="control-hooks"
        layout="vertical"
        onFieldsChange={handleChangeForm}
        onFinish={(values) => handleUpdateOrCreateModuleName(values, isUpdate)}
      >
        <div className="d-flex w-100 justify-content-end">
          <Popconfirm
            cancelText={t('Cancel')}
            okText={t('Ok')}
            title={t('Möchten Sie den Eintrag wirklich löschen?')}
            onConfirm={handleDeleteModuleName}
          >
            <Button danger type="text">
              <DeleteFilled />
            </Button>
          </Popconfirm>
        </div>
        <div id="profile">
          <Suspense fallback={EmptyLoader}>
            {/* ADD FORM HERE */}
            <ModuleNameForm />
          </Suspense>
        </div>

        <Form.Item>
          <div className="d-flex w-100 justify-content-end">
            <Space>
              <Button onClick={handleToggleAddOrUpdateShowModuleNameModal} type="default">
                {t('Cancel')}
              </Button>
              <Button loading={isCreating} type="primary" htmlType="submit">
                {t(isUpdate ? t('Update') : t('Submit'))}
              </Button>
            </Space>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default inject('store')(observer(AddOrUpdateModuleNameModal));
