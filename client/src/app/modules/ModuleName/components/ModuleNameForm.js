import React from 'react';
import { Col, Form, Input, InputNumber, Row, DatePicker, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import InputMask from 'react-input-mask';
import { inject, observer } from 'mobx-react';

/**CORE IMPORTS */
import { numeric, required } from '@core_common/antdhelpers/helperfunctions';
import { TWO_GRID } from '@core_common/antdhelpers/constants';

//APP IMPORTS
import { ModuleNameController } from '@app_modules/ModuleName/controller';

const { TextArea } = Input;

function ModuleNameForm({ store }) {
  const { t } = useTranslation('common');
  const { handleGetDateFormat } = ModuleNameController({ store });

  return (
    <Row gutter={[16, 0]}>
      <Col {...TWO_GRID}>
        <Form.Item name="name" label={t('Normal Input')} rules={[required(t('Please enter name'))]}>
          <Input className="w-100" placeholder={t('Normal Input')} />
        </Form.Item>
      </Col>
      <Col {...TWO_GRID}>
        <Form.Item
          name="amount"
          label={t('Input Number')}
          rules={[required(t('Please enter amount')), numeric(t('Amount must be numeric [LME]'))]}
        >
          <InputNumber className="w-100" placeholder={t('Input Numbe')} />
        </Form.Item>
      </Col>
      <Col {...TWO_GRID}>
        <Form.Item
          name="inputmask"
          label={t('Hour input')}
          rules={[required(t('Please enter amount')), numeric(t('Amount must be numeric [LME]'))]}
        >
          <InputMask placeholder="HH:mm" className="ant-input w-100" mask={'99:99'} />
        </Form.Item>
      </Col>
      <Col {...TWO_GRID}>
        <Form.Item
          name="event_date"
          label={t('Date')}
          // rules={[required(t('Please enter Event Date'))]}
        >
          <DatePicker placeholder={t('Date')} format={handleGetDateFormat()} className="w-100" />
        </Form.Item>
      </Col>
      <Col {...TWO_GRID}>
        <Form.Item
          className="w-100"
          name="wholesaler"
          label={t('Dropdown')}
          rules={[required(t('Please enter wholesaler'))]}
        >
          <Select showSearch={true} placeholder={t('Select wholesaler')} className="w-100">
            <Option value={'Wholesaler A'}>Wholesaler A</Option>
            <Option value={'Wholesaler B'}>Wholesaler B</Option>
            <Option value={'Wholesaler C'}>Wholesaler C</Option>
          </Select>
        </Form.Item>
      </Col>
      <Col {...TWO_GRID}>
        <Form.Item
          name="description"
          label={t('Text area')}
          rules={[required(t('Please enter description'))]}
        >
          <TextArea rows={4} className="w-100" placeholder={t('Description')} />
        </Form.Item>
      </Col>{' '}
    </Row>
  );
}

export default inject('store')(observer(ModuleNameForm));
