import React, { Suspense } from 'react';
import { Button, Col, Input, Popconfirm, Row, Space, Tooltip } from 'antd';
import { inject, observer } from 'mobx-react';
import { PlusOutlined, DeleteOutlined, DownloadOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactTitle, FallBackLoaders } from '@core_common/components';
import { AddOrUpdateModuleNameModal } from '@app_modules/ModuleName/components';
import { ModuleNameController } from '@app_modules/ModuleName/controller';
import { ANTD_HALF_COL } from '@core_common/antdhelpers/constants';

function ModuleNameHeader({ isDeleteVisible, setIsDeleteVisible, store, form }) {
  const { t } = useTranslation('common');

  const {
    handleToggleAddOrUpdateShowModuleNameModal,
    handleModuleNameSearch,
    handleDeleteMultiple,
    handleExportData
  } = ModuleNameController({
    store,
    setIsDeleteVisible,
    t,
    form
  });

  return (
    <>
      <Row>
        <Col md={{ span: ANTD_HALF_COL }}>
          {' '}
          <ExactTitle level={3} text={t('ModuleName')} />
        </Col>
        <Col
          className="d-flex w-100 justify-content-end align-items-center"
          md={{ span: ANTD_HALF_COL }}
        >
          {' '}
          <Space className="mb-3">
            {isDeleteVisible && (
              <Popconfirm
                onConfirm={handleDeleteMultiple}
                title={t('Are you sure?')}
                okText={t('Yes')}
                cancelText={t('No')}
              >
                <Button className="shadow-sm" type="primary" danger icon={<DeleteOutlined />} />
              </Popconfirm>
            )}

            <Button
              onClick={() => handleToggleAddOrUpdateShowModuleNameModal()}
              className="shadow-sm"
              type="default"
              icon={<PlusOutlined />}
            />

            <Tooltip title={t('EXPORT DATA TO CSV')}>
              <Button
                onClick={handleExportData}
                className="shadow-sm"
                loading={store.ModuleNameManagementUtilities.isDownloading}
                type="default"
                icon={<DownloadOutlined />}
              />
            </Tooltip>

            <Input.Search
              className="shadow-sm"
              onSearch={handleModuleNameSearch}
              id="exact-search"
              placeholder={t('Search')}
            />
          </Space>
        </Col>
      </Row>

      <Suspense fallback={FallBackLoaders.EmptyLoader}>
        <AddOrUpdateModuleNameModal form={form} />
      </Suspense>
    </>
  );
}

export default inject('store')(observer(ModuleNameHeader));
