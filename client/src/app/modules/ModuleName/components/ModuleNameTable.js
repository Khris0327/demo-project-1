import React, { useEffect } from 'react';

import { Button, Divider, Space, Switch, Table, Typography } from 'antd';
import { EditFilled, MailFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';

/**CORE IMPORTS */
import { ModuleNameController } from '@app_modules/ModuleName/controller';

const { Text } = Typography;

function ModuleNameTable({ store, form, setIsDeleteVisible }) {
  const { t } = useTranslation('common');

  const {
    getModuleNames,
    onChangePage,
    handleToggleAddOrUpdateShowModuleNameModal,
    handleSetSelectedRowKeys
  } = ModuleNameController({ store, form, t });

  // eslint-disable-next-line
  useEffect(getModuleNames, []);

  const columns = [
    {
      title: t('Rückvergütungen Data'),
      render: (ModuleNameData) => (
        <Space
          split={<Divider style={{ paddingTop: 10, paddingBottom: 10, margin: 0 }} />}
          direction="vertical"
        >
          <Space direction="vertical">
            <Text type="secondary">{t('Whole Saler')}</Text>
            <Text strong>{ModuleNameData.wholesaler}</Text>
          </Space>

          <Space direction="vertical">
            <Text type="secondary">{t('Customer ID')}</Text>
            <Text strong>{ModuleNameData.customer_id}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Brand')}</Text>
            <Text strong>{ModuleNameData.brand}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Product')}</Text>
            <Text strong>{ModuleNameData.product}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Quantity')}</Text>
            <Text strong>{ModuleNameData.quantity}</Text>
          </Space>
          <Space direction="vertical">
            <Text type="secondary">{t('Refund')}</Text>
            <Text strong>{ModuleNameData.refund}</Text>
          </Space>

          <Space>
            <Button
              onClick={() => handleToggleAddOrUpdateShowModuleNameModal(true, ModuleNameData)}
              icon={<EditFilled />}
              type="link"
            >
              {t('Update')}
            </Button>
          </Space>
        </Space>
      ),
      responsive: ['xs']
    },
    {
      title: t('Wholesaler').toUpperCase(),
      dataIndex: 'wholesaler',
      render: (text) => <>{text}</>,
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      sorter: (a, b) => a.wholesaler.toLowerCase().localeCompare(b.wholesaler.toLowerCase()),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Customer ID').toUpperCase(),
      dataIndex: 'customer_id',
      sorter: (a, b) => a.customer_id.toLowerCase().localeCompare(b.customer_id.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Brand').toUpperCase(),
      dataIndex: 'brand',

      sorter: (a, b) => a.brand.toLowerCase().localeCompare(b.brand.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Product').toUpperCase(),
      dataIndex: 'product',

      sorter: (a, b) => a.product.toLowerCase().localeCompare(b.product.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Quantity [LME]').toUpperCase(),
      dataIndex: 'quantity',

      sorter: (a, b) => a.quantity.toLowerCase().localeCompare(b.quantity.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Pharmacode').toUpperCase(),
      dataIndex: 'pharmacode',

      sorter: (a, b) => a.pharmacode.toLowerCase().localeCompare(b.pharmacode.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },
    {
      title: t('Refund [CHF]').toUpperCase(),
      dataIndex: 'refund',

      sorter: (a, b) => a.refund.toLowerCase().localeCompare(b.refund.toLowerCase()),
      shouldCellUpdate: (prev, next) => JSON.stringify(prev) !== JSON.stringify(next),
      responsive: ['sm', 'md', 'lg', 'xl']
    },

    {
      title: t('Action').toUpperCase(),
      dataIndex: 'action',
      responsive: ['lg', 'xl'],
      shouldCellUpdate: (prev, next) => false,
      render: (_, ModuleNameData) => {
        //console.log(ModuleNameData.toJSON(), 'ModuleNameData');

        return (
          <>
            <Button
              onClick={() => {
                handleToggleAddOrUpdateShowModuleNameModal(true, ModuleNameData);
              }}
              icon={<EditFilled />}
              type="link"
            />
          </>
        );
      }
    }
  ];

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      let hasChecked = selectedRows.length > 0;

      setIsDeleteVisible(hasChecked);
      handleSetSelectedRowKeys(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      firstName: record.name
    })
  };

  return (
    <Table
      loading={store.moduleName.loading}
      className="exact-table shadow-sm bg-white p-3"
      size="small"
      locale={{
        triggerDesc: t('Click to sort descending'),
        triggerAsc: t('Click to sort ascending'),
        cancelSort: t('Click to cancel sorting')
      }}
      rowSelection={{
        type: 'checkbox',
        ...rowSelection
      }}
      columns={columns}
      dataSource={store.moduleName.state.toJSON()}
      pagination={{
        position: ['bottomCenter'],
        hideOnSinglePage: true,
        pageSize: 10,
        responsive: true,
        onChange: onChangePage,
        showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} ModuleNames`,
        // pageSizeOptions: [5, 10, 15, 20],
        showSizeChanger: false,
        total: store.moduleName.toJSON().total
      }}
    />
  );
}

export default inject('store')(observer(ModuleNameTable));
