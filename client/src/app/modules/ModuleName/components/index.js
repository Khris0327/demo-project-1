import { lazy } from 'react';

const ModuleNameHeader = lazy(() => import('./ModuleNameHeader'));
const ModuleNameTable = lazy(() => import('./ModuleNameTable'));
const AddOrUpdateModuleNameModal = lazy(() => import('./AddOrUpdateModuleNameModal'));
const ModuleNameForm = lazy(() => import('./ModuleNameForm'));

export { ModuleNameHeader, ModuleNameTable, AddOrUpdateModuleNameModal, ModuleNameForm };
