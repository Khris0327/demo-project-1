import { message } from 'antd';

const ModuleNameController = ({ store, form, t, setIsDeleteVisible }) => {
  const getModuleNames = async () => {
    const page = 1;

    const search = store.ModuleNameManagementUtilities.search;
    const props = store.ModuleNameManagementUtilities.props;
    let params = { page, search, props, user_type: store.login.type, user_id: store.login.id };

    await store.moduleName.LIST(params);
  };

  const onChangePage = async (page) => {
    const search = store.ModuleNameManagementUtilities.search;
    const props = store.ModuleNameManagementUtilities.props;

    let params = { page, search, props, user_type: store.login.type, user_id: store.login.id };

    await store.moduleName.LIST(params);
  };

  const handleToggleAddOrUpdateShowModuleNameModal = (isUpdate = false, ModuleNameData = null) => {
    form.resetFields();
    if (isUpdate) {
      ModuleNameData = store.moduleName.state.find((c) => c.id === ModuleNameData?.id);
      form.setFieldsValue(ModuleNameData);
      store.ModuleNameManagementUtilities.setUpdateId(ModuleNameData?.id);
    }

    store.ModuleNameManagementUtilities.setToggleShowAddOrUpdateModuleNameModal(isUpdate);
  };
  const handleChangeForm = (values) => {};

  const addUserId = (values) => {
    values.user_id = store.login.id;
    return values;
  };

  const handleDeleteMultiple = async () => {
    const selectedRowIds = store.ModuleNameManagementUtilities.selectedRowsKeys;
    const currentPage = store.ModuleNameManagementUtilities.currentPage;
    await store.moduleName.DELETE(selectedRowIds);
    setIsDeleteVisible(false);
    await onChangePage(currentPage);
  };

  const handleUpdateOrCreateModuleName = async (values, isUpdate, isSelf = false) => {
    values = addUserId(values);

    message.loading({
      content: isUpdate ? t('Updating ModuleName') : t('Creating ModuleName'),
      key: 'creatingModuleName'
    });

    const params = isUpdate
      ? [store.ModuleNameManagementUtilities.updateId, values] // If updating self use id in login store
      : [values];

    let [successMessage, error] = await store.moduleName[isUpdate ? 'UPDATE' : 'CREATE'](...params);

    if (!isUpdate) {
      store.users.setTotal(store.users.total + 1);
    }

    let success = await _showresultMessage(error, {
      successMessage: t(successMessage.message) // isUpdate ? t('ModuleName Updated') :
    });

    if (success) {
      if (!isSelf) {
        form.resetFields();
        store.ModuleNameManagementUtilities.setToggleShowAddOrUpdateModuleNameModal();
      }
    }
  };

  const handleFetchModuleNameTypes = async () => {
    await store.ModuleNameManagementUtilities.FETCH_ModuleName_TYPES();
  };

  const deleteFile = async () => {
    store.moduleName.deleteFile();
  };

  const handleExportData = async () => {
    store.ModuleNameManagementUtilities.setIsDownloading(true);

    let response = await store.moduleName.exportData();

    if (response.data) {
      response.data = _changeCSVSeparator(response.data);

      const url = window.URL.createObjectURL(
        new Blob(['\ufeff', response.data], {
          type: response.headers['content-type'] + ';text/csv;charset=utf-8'
        })
      );
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `export_file.csv`); //or any other extension
      document.body.appendChild(link);
      link.click();
      await deleteFile();

      store.ModuleNameManagementUtilities.setIsDownloading(false);
    }
  };

  const handleModuleNameSearch = async (search) => {
    search = search.trim();
    const props = store.ModuleNameManagementUtilities.props;
    let params = { search, props, user_type: store.login.type, user_id: store.login.id };

    store.ModuleNameManagementUtilities.setSearch(search);
    await store.moduleName.LIST(params);
  };

  const handleChangeModuleNameStatus = async (params) => {
    let { ModuleNameData, active } = params;

    let [successMessage, error] = await store.moduleName.UPDATE(ModuleNameData.id, {
      active
    });

    await _showresultMessage(error, {
      successMessage: successMessage.message
    });
  };

  const handleSendEmailToModuleName = async (ModuleNameData) => {
    message.loading({
      content: t('Sending Email'),
      key: 'creatingModuleName'
    });

    let [successResponse, error] = await store.moduleName.SEND_EMAIL({
      id: ModuleNameData.id,
      email: ModuleNameData.email
    });

    await _showresultMessage(error, {
      successMessage: t(successResponse.message)
    });
  };

  const handleSetSelectedRowKeys = async (setSelectedRowsKeys) => {
    store.ModuleNameManagementUtilities.setSelectedRowsKeys(setSelectedRowsKeys);
  };

  const handleDeleteModuleName = async () => {
    let data = form.getFieldValue();
    await store.moduleName.DELETE(data.id);
    getModuleNames();

    store.ModuleNameManagementUtilities.setToggleShowAddOrUpdateModuleNameModal();
    form.resetFields();
  };

  const handleGetDateFormat = () => {
    return store.ModuleNameManagementUtilities.getDateFormat(store.globalState.language);
  };

  async function _showresultMessage(error, { successMessage }) {
    return new Promise((resolve, reject) => {
      if (error) {
        message.error({
          content: t(error.response.data.error.message),
          key: 'creatingModuleName'
        });

        reject(false);
      } else {
        message.success({ content: successMessage, key: 'creatingModuleName' });
        resolve(true);
      }
    });
  }

  function _changeCSVSeparator(data) {
    let locale = store.globalState.language;

    switch (locale) {
      case 'de':
        return data.replace(/,/g, ';');
      default:
        return data;
    }
  }

  return {
    getModuleNames,
    onChangePage,
    handleToggleAddOrUpdateShowModuleNameModal,
    handleChangeForm,
    handleUpdateOrCreateModuleName,
    handleFetchModuleNameTypes,
    handleModuleNameSearch,
    handleChangeModuleNameStatus,
    handleSendEmailToModuleName,
    handleDeleteModuleName,
    handleDeleteMultiple,
    handleSetSelectedRowKeys,
    handleExportData,
    handleGetDateFormat
  };
};

export default ModuleNameController;
