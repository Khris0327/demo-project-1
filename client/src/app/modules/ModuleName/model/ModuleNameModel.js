import { types } from 'mobx-state-tree';

const ModuleNameModel = types
  .model('ModuleNameModel', {
    id: types.optional(types.integer, 0),
    key: types.optional(types.integer, 0)
  })
  .views((self) => ({}))
  .actions((self) => ({}));

export default ModuleNameModel;
