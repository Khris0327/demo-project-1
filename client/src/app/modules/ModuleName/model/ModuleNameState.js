import { types, flow } from 'mobx-state-tree';
import ModuleNameModel from './ModuleNameModel';
import ApplyRootCrud from '@core_state_management/Helpers/RootCrud';
import axios from 'axios';

const API_PATH = '/api/module_name';
const API_EXPORT_PATH = '/api/module_name/export_csv';

export default types
  .model('ModuleNameState', {
    state: types.optional(types.array(ModuleNameModel), []),
    singleState: types.optional(ModuleNameModel, {}),
    total: types.optional(types.number, 0),
    loading: types.optional(types.boolean, false),
    isCreating: types.optional(types.boolean, false)
  })
  .actions((self) => ({
    ...ApplyRootCrud(API_PATH, self),

    setTotal: (total) => {
      self.total = total;
    },

    exportData: flow(function* () {
      return yield axios.get(API_EXPORT_PATH);
    }),

    getCSV: flow(function* () {
      return yield axios({
        url: '/public/export/export_file.csv', //your url
        method: 'GET',
        responseType: 'blob' // important
      });
    }),

    deleteFile: flow(function* () {
      yield axios.delete('/api/feedback/delete_csv');
    })
  }))
  .views((self) => ({}));
