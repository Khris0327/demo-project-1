import { FeedbackState, FeedbackManagementUtilities } from '@app_modules/Feedback/model';
import { types } from 'mobx-state-tree';

const AppStore = {
  feedback: types.optional(FeedbackState, {}),
  FeedbackManagementUtilities: types.optional(FeedbackManagementUtilities, {})
};

export default AppStore;
