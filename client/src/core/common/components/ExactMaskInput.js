import React from 'react';

//FOR TIME MASK use "'99:99'"
function ExactMaskInput({ placeholder, mask, ...rest }) {
  return <InputMask {...rest} placeholder={placeholder} className="ant-input w-100" mask={mask} />;
}

export default ExactMaskInput;
