import React from 'react';
import { Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import { DropdownSettingsController } from '../controller';
import { useTranslation } from 'react-i18next';

const { Dragger } = Upload;

function AddDropdowns({ instruction }) {
  const { t } = useTranslation('common');
  const { handleUploadingCSV } = DropdownSettingsController({ t });

  const props = {
    name: 'file',
    multiple: false,
    action: '/api/dropdown_settings',
    onChange: handleUploadingCSV,
    showUploadList: false,
    onDrop(e) {}
  };
  return (
    <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">{instruction}</p>
    </Dragger>
  );
}

export default AddDropdowns;
