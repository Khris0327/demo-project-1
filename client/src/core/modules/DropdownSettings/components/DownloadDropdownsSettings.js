import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'antd';

/**CORE IMPORTS */
import { DropdownSettingsController } from '@core_modules/DropdownSettings/controller';
import { inject } from 'mobx-react';
import { observer } from 'mobx-react-lite';

function DownloadDropdownsSettings({ store }) {
  const { t } = useTranslation('common');

  const { handleDownload } = DropdownSettingsController({ store });

  return (
    <div className="d-flex align-items-center justify-content-center w-100 h-100">
      <Button
        loading={store.dropdownSettings.isDownloading}
        onClick={handleDownload}
        type="primary"
        size="large"
      >
        {t('DropdownSetting_#Download Settings')}
      </Button>
    </div>
  );
}

export default inject('store')(observer(DownloadDropdownsSettings));
