import React, { Suspense } from 'react';
import { Alert, Col, Collapse, Row, Space, Typography, List } from 'antd';

/** CORE IMPORTS*/
import { TWO_GRID, FULL_GRID } from '@core_common/antdhelpers/constants';
import { FallBackLoaders } from '@core_common/components';
import {
  AddOrUpdateDropdowns,
  DownloadDropdownsSettings
} from '@core_modules/DropdownSettings/components';
import { useTranslation } from 'react-i18next';

const { Text } = Typography;
const { Panel } = Collapse;

function SettingsButtons() {
  const { t } = useTranslation('common');
  const alpha = 'abcdefghijklmnopqrstuvwxyz';

  const data = [
    { itemText: "DropdownSetting_#Click on 'Download Dropdowns'", itemChild: [] },
    {
      itemText:
        'DropdownSetting_#An Excel file will open, in which you will find the following information',
      itemChild: [
        { itemText: "DropdownSetting_#'id' Technical numbering, irrelevant" },
        {
          itemText:
            "DropdownSetting_#'dropdown_type' name of the dropdown field'. You can use this to find the dropdown that is relevant for you"
        },
        {
          itemText:
            "DropdownSetting_#'entry' These are the actual choices for the dropdown. You can change this by either making textual adjustments or adding a new entry. Note, If you want to delete an entry, change the number in column 'status' from 1 to 0. This deactivates the entry (i.e. it is no longer visible in the dropdown), but without deleting it"
        },
        {
          itemText:
            "DropdownSetting_#'value' This field is only needed if the dropdown selection is relevant for a calculation. In this case it can be filled with a value"
        },
        {
          itemText:
            "DropdownSetting_#'status' Indicates whether an entry is displayed in the tool for selection or not. 1 = active (i.e. is displayed), 0 = inactive (i.e. is not displayed). If you want to delete an entry, you can adjust the status accordingly"
        }
      ]
    },
    {
      itemChild: [],
      itemText:
        'DropdownSetting_#Once you have made the adjustments, save the Excel file locally and drag it into the field provided. The file is loaded and the drop-down selections in the tool are adjusted'
    },
    {
      itemChild: [],
      itemText:
        'DropdownSetting_#If you receive an error message during the upload, please check if steps 1-3 have been performed correctly, especially if no (unwanted) changes have been made to the file (e.g. deletion of a line, invalid status, etc.)'
    }
  ];

  return (
    <div>
      <Row gutter={[16, 16]}>
        <Col {...FULL_GRID}>
          <Collapse defaultActiveKey={[]}>
            <Panel op header={t('DropdownSetting_#Click here for more info')} key="1">
              <Space direction="vertical">
                <Alert
                  message={
                    <>
                      <Space direction="vertical">
                        <Text strong>{t('DropdownSetting_#Adding new choices for dropdowns')}</Text>
                        <Text strong>
                          {t(
                            'DropdownSetting_#If you want to add new dropdown selections, proceed as follows'
                          )}
                        </Text>
                      </Space>
                    </>
                  }
                  type="info"
                  description={
                    <>
                      <List
                        className="w-100"
                        header={<Text strong>{t('Note')}</Text>}
                        bordered
                        dataSource={data}
                        renderItem={(item, idx) => (
                          <List.Item>
                            <Space direction="vertical">
                              <Text>
                                {idx + 1}) {t(item.itemText)}
                              </Text>
                              {item.itemChild.length > 0 && (
                                <List
                                  className="w-100"
                                  bordered
                                  dataSource={item.itemChild}
                                  renderItem={(item, idx) => (
                                    <List.Item>
                                      <Text>
                                        {alpha[idx]}) {t(item.itemText)}
                                      </Text>
                                    </List.Item>
                                  )}
                                />
                              )}
                            </Space>
                          </List.Item>
                        )}
                      />
                    </>
                    //   t(
                    //   'If you want to add new dropdown data, just fill out the dropdown_type, entry, value and status column, we will automatically generate an ID for that data'
                    // )
                  }
                />

                {/* <Alert
                  message={<Text strong>{t('Adding dropdown setting data')}</Text>}
                  type="info"
                  description={t(
                    'If you want to add new dropdown data, just fill out the dropdown_type, entry, value and status column, we will automatically generate an ID for that data'
                  )}
                />
                <Alert
                  message={<Text strong>{t('Updating dropdown setting data')}:</Text>}
                  description={t(
                    'You can change any data in the dropdown_type, entry, value and status column but do not change the id column'
                  )}
                  type="info"
                /> */}
                {/* <List
                  className="w-100"
                  header={<Text strong>{t('Note')}</Text>}
                  bordered
                  dataSource={data}
                  renderItem={(item, idx) => (
                    <List.Item>
                      <Text>
                        {idx + 1}) {t(item)}
                      </Text>
                    </List.Item>
                  )}
                /> */}
              </Space>
            </Panel>
          </Collapse>
        </Col>
        <Col {...TWO_GRID}>
          <Suspense fallback={FallBackLoaders.EmptyLoader}>
            <AddOrUpdateDropdowns
              instruction={t(
                'DropdownSetting_#Click or drag csv file to this area to upload new dropdown settings'
              )}
            />
          </Suspense>
        </Col>
        <Col {...TWO_GRID}>
          <Suspense fallback={FallBackLoaders.EmptyLoader}>
            <DownloadDropdownsSettings />
          </Suspense>
        </Col>
      </Row>
    </div>
  );
}

export default SettingsButtons;
