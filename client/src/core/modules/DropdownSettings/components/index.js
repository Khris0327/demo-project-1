import { lazy } from 'react';

const SettingsButtons = lazy(() => import('./SettingsButtons'));
const AddOrUpdateDropdowns = lazy(() => import('./AddOrUpdateDropdowns'));
const DownloadDropdownsSettings = lazy(() => import('./DownloadDropdownsSettings'));

export { SettingsButtons, AddOrUpdateDropdowns, DownloadDropdownsSettings };
