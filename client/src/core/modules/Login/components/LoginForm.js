import React, { useEffect } from 'react';
import { inject, observer } from 'mobx-react';
import { Alert, Button, Checkbox, Form, Input, Typography, Dropdown } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

/*CORE IMPORTS */
import { ExactText } from '@core_common/components';
import { email, required } from '@core_common/antdhelpers/helperfunctions';
import { Routes } from '@core_routing/';
import { LoginController } from '@core_modules/Login/controller';

import { OTPModal } from '@core_modules/Login/components';
import { UserMenu } from '@core_modules/Login/components';
//Flag

const { Text } = Typography;

function LoginForm({ store }) {
  const history = useHistory();
  const { globalState } = store;

  const [form] = Form.useForm();

  const { t } = useTranslation('common');
  const { handleLogin, csrfProtect } = LoginController({ store, history, t });

  // eslint-disable-next-line
  useEffect(csrfProtect, []);

  return (
    <>
      <div style={{ position: 'absolute', top: 10, right: 15 }}>
        <Dropdown overlay={<UserMenu />} placement="topRight" arrow>
          <Text>{globalState.language.toUpperCase()}</Text>
        </Dropdown>
      </div>
      <div className="mb-4 text-center">
        <Text>{t('Login_Form_#Enter your email address and password to login')}</Text>
        {store.login.loginErrorMessage && (
          <Alert className="mt-3" message={t(store.login.loginErrorMessage)} type="error" />
        )}
      </div>
      <Form
        form={form}
        name="control-hooks"
        layout="vertical"
        onFinish={handleLogin}
        initialValues={{ email: 'test.user@exact-construct.ch', password: 'DVuqUcHqWT' }}
      >
        <Form.Item
          className="w-100"
          name="email"
          label={t('Login_Form_#Email Address')}
          rules={[
            required(t('Login_Form_#Please input your email address!')),
            email(t('Login_Form_#Please input a valid email address'))
          ]}
        >
          <Input
            autoComplete="off"
            autoCorrect="off"
            autoFocus="on"
            className="w-100"
            placeholder={t('Login_Form_#Email Address')}
          />
        </Form.Item>
        <Form.Item
          className="w-100"
          name="password"
          label={t('Login_Form_#Password')}
          rules={[required(t('Login_Form_#Please input your password!'))]}
        >
          <Input.Password visibilityToggle={false} className="w-100" placeholder={t('Login_Form_#Password')} />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>{t('Login_Form_#Remember Me')}</Checkbox>
        </Form.Item>
        <Form.Item name="remember" valuePropName="checked">
          <Button
            loading={store.login.isLoggingIn}
            className="w-100"
            htmlType="submit"
            type="primary"
          >
            {store.login.isLoggingIn ? t('Login_Form_#Logging in') : t('Login_Form_#Login')}
          </Button>
        </Form.Item>

        <div className="text-center">
          <Link to={Routes.RESET_PASSWORD_ROUTE}>
            <ExactText text={t('Login_Form_#Forgot password')} />
          </Link>
        </div>
      </Form>

      <OTPModal />
    </>
  );
}

export default inject('store')(observer(LoginForm));
