import { lazy } from 'react';
import UserMenu from './UserMenu';

const LoginForm = lazy(() => import('./LoginForm'));

const OTPModal = lazy(() => import('./OTPModal'));

export { LoginForm, OTPModal, UserMenu };
