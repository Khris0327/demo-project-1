const UserMenuController = ({ store, i18n }) => {
  const changeLanguage = (locale) => {
    store.globalState.setLanguage(locale);
  };

  return { changeLanguage };
};

export default UserMenuController;
