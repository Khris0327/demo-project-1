import React, { useEffect, useRef, useContext, useState } from 'react';

import { Input, Form } from 'antd';

import { EditableContext } from '@core_modules/Translation/components/EditableRow';
import { useTranslation } from 'react-i18next';

function EditableCell({ title, editable, children, dataIndex, record, handleSave, ...restProps }) {
  const { t } = useTranslation('common');
  const [editing, setEditing] = useState(false);
  const inputRef = useRef(null);
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex]
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;
  const isRequiredMessage = t('Translation_Table_#is required');

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: t(`${title} ${isRequiredMessage}`)
          }
        ]}
      >
        <Input.TextArea ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
}

export default EditableCell;
