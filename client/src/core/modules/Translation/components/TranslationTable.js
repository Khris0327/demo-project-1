import React, { useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';
import { Table } from 'antd';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { EditableCell, EditableRow } from '@core_modules/Translation/components';
import { TranslationController } from '@core_modules/Translation/controller';
import { ExactCard } from '@core_common/components';

function TranslationTable({
  store,
  searchKeyword,
  searchProperties,
  count,
  setCount,
  modalVisible
}) {
  const { t } = useTranslation('common');
  const [columnLocales, setColumnLocales] = useState([]);

  let columns = [
    {
      title: 'Key',
      dataIndex: 'translation_key',
      width: '25%',
      editable: false
    },
    ...columnLocales
  ];

  const { generateColumns, getTranslations, search, generateLocaleColumns } = TranslationController(
    {
      setCount,
      count,
      store,
      t,
      setColumnLocales
    }
  );
  columns = [
    {
      title: 'Key',
      dataIndex: 'translation_key',
      width: '25%',
      editable: false
    },
    ...columnLocales
  ];

  columns = generateColumns(columns);
  // eslint-disable-next-line
  useEffect(getTranslations, []);

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell
    }
  };

  useEffect(() => {
    if (!modalVisible) {
      generateLocaleColumns();
    }
    // eslint-disable-next-line
  }, [modalVisible, store.translations.isUpdateKey]);

  return (
    <div>
      <ExactCard>
        <Table
          loading={store.translations.isLoading}
          className="exact-table"
          size="small"
          components={components}
          rowClassName={() => 'editable-row'}
          dataSource={store?.translations?.state
            ?.toJSON()
            .filter((e) => search(searchProperties, e.toJSON())(searchKeyword))}
          columns={columns}
          pagination={{
            position: ['bottomCenter'],
            hideOnSinglePage: true,
            pageSize: 10,
            responsive: true,
            showTotal: (total, range) => {
              return (
                <>
                  {range[0]}-{range[1]} {t('UserManagement_Table_#of')} {total}{' '}
                  {t('UserManagement_Table_#users')}
                </>
              );
            },
            showSizeChanger: false,
            total: store.users.toJSON().total
          }}
        />
      </ExactCard>
    </div>
  );
}

export default inject('store')(observer(TranslationTable));
