import React, { Suspense, useEffect } from 'react';
import { Form, Modal, Space, Button, Checkbox } from 'antd';
import { inject, observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactText, FallBackLoaders } from '@core_common/components';
import { UserManagementController } from '@core_modules/UserManagement/controller';
import { UserTypeForm, ProfileForm, AddressForm } from '@core_modules/UserManagement/components';

function AddOrUpdateUserModal({ store, form }) {
  const { t } = useTranslation('common');

  const { handleToggleAddOrUpdateShowUserModal, handleUpdateOrCreateUser, csrfProtect } =
    UserManagementController({
      store,
      form,
      t
    });

  useEffect(csrfProtect, []);

  const { isUpdate, showAddOrUpdateUserModal } = store.UserManagementUtilities;
  const { isCreating } = store.users;
  const { EmptyLoader } = FallBackLoaders;

  return (
    <Modal
      title={
        <ExactText
          text={
            isUpdate
              ? t('UserManagement_UpdateAndAdd_#Update User')
              : t('UserManagement_UpdateAndAdd_#Add New User')
          }
        />
      }
      visible={showAddOrUpdateUserModal}
      footer={null}
      className="exact-modal"
      style={{ top: 20 }}
      onCancel={handleToggleAddOrUpdateShowUserModal}
    >
      <Form
        form={form}
        name="control-hooks"
        layout="vertical"
        autoComplete="off"
        onFinish={(values) => handleUpdateOrCreateUser(values, isUpdate)}
      >
        <div id="profile">
          <Suspense fallback={EmptyLoader}>
            <UserTypeForm isUpdate={isUpdate} />
          </Suspense>
          <Suspense fallback={EmptyLoader}>
            <ProfileForm isUpdate={isUpdate} isAdmin={store.login.isAdmin} />
          </Suspense>
          <Suspense fallback={EmptyLoader}>
            <AddressForm />
          </Suspense>
        </div>

        <Form.Item name="sendInviteEmail" valuePropName="checked">
          <Checkbox disabled={!store.login.isAdmin}>
            {t('UserManagement_UpdateAndAdd_#Send Invitation Email')}
          </Checkbox>
        </Form.Item>
        <Form.Item>
          <div className="d-flex w-100 justify-content-end">
            <Space>
              <Button onClick={handleToggleAddOrUpdateShowUserModal} type="default">
                {t('UserManagement_UpdateAndAdd_#Cancel')}
              </Button>
              <Button loading={isCreating} type="primary" htmlType="submit">
                {t(
                  isUpdate
                    ? t('UserManagement_UpdateAndAdd_#Update')
                    : t('UserManagement_UpdateAndAdd_#Submit')
                )}
              </Button>
            </Space>
          </div>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default inject('store')(observer(AddOrUpdateUserModal));
