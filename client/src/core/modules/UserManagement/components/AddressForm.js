import React from 'react';
import { Col, Form, Input, Row, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';

import CountryData from '@core_data/countries/Countries.json';

/**CORE IMPORTS */
import { ExactText } from '@core_common/components';
import { required } from '@core_common/antdhelpers/helperfunctions';

import { TWO_GRID } from '@core_common/antdhelpers/constants';

const { Option } = Select;

function AddressForm({ store }) {
  const { t } = useTranslation('common');

  return (
    <>
      <div className="mb-2">
        <ExactText text={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Address')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            name="country"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Country')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter country'))
            ]}
          >
            <Select
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Select A Country')}
              showSearch={true}
              className="w-100"
            >
              {CountryData[store.globalState.language].map((row, index) => (
                <Option key={row.name} value={row.name}>
                  {row.name}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="city"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#City')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter city'))
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Enter City')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="street_nr"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Street')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter Street'))
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Street')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="zip_code"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#ZIP')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter zip code'))
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#ZIP')}
            />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}

export default inject('store')(observer(AddressForm));
