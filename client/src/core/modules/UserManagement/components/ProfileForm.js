import React from 'react';
import { Col, Form, Input, Row } from 'antd';
import { useTranslation } from 'react-i18next';

/**CORE IMPORTS */
import { ExactText } from '@core_common/components';
import { email, required } from '@core_common/antdhelpers/helperfunctions';
import { TWO_GRID } from '@core_common/antdhelpers/constants';

function ProfileForm({ isAdmin, isUpdate }) {
  const { t } = useTranslation('common');

  return (
    <>
      <div className="mb-2">
        <ExactText text={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Profile')} />
      </div>
      <Row gutter={[16, 0]}>
        <Col {...TWO_GRID}>
          <Form.Item
            className="w-100"
            name="first_name"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#First name')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter first name'))
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#First name')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="last_name"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Last Name')}
            rules={[
              required(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter last name'))
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Last Name')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="email"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Email Address')}
            rules={[
              required(
                t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter email address')
              ),
              email(t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter a valid email'))
            ]}
          >
            <Input
              disabled={!isAdmin && isUpdate}
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Email Address')}
            />
          </Form.Item>
        </Col>
        <Col {...TWO_GRID}>
          <Form.Item
            name="phone_number"
            label={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Phone Number')}
            rules={[
              required(
                t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Please enter phone number')
              )
            ]}
          >
            <Input
              className="w-100"
              placeholder={t('UserManagement_UpdateAndAdd_AND_AccountSetting_#Phone Number')}
            />
          </Form.Item>
        </Col>
      </Row>
    </>
  );
}

export default ProfileForm;
