import React, { Suspense } from 'react';
import { Button, Col, Input, Row, Space, Popconfirm } from 'antd';
import { inject, observer } from 'mobx-react';
import { PlusOutlined, DownloadOutlined, DeleteOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';

/**COMPONENTS */

/**CORE IMPORTS */
import { ExactTitle, FallBackLoaders } from '@core_common/components';
import { AddOrUpdateUserModal } from '@core_modules/UserManagement/components';
import { UserManagementController } from '@core_modules/UserManagement/controller';
import { ANTD_HALF_COL } from '@core_common/antdhelpers/constants';

function UserManagementHeader({ isDeleteVisible, setIsDeleteVisible, store, form }) {
  const { t } = useTranslation('common');
  const {
    handleToggleAddOrUpdateShowUserModal,
    handleUserSearch,
    handleExportData,
    handleOnChangeSearch,
    handleDeleteMultiple
  } = UserManagementController({
    store,
    setIsDeleteVisible,
    t,
    form
  });

  return (
    <>
      <Row>
        <Col md={{ span: ANTD_HALF_COL }}>
          {' '}
          <ExactTitle level={3} text={t('UserManagement_Header_#User Management')} />
        </Col>
        <Col
          className="d-flex w-100 justify-content-end align-items-center"
          md={{ span: ANTD_HALF_COL }}
        >
          {' '}
          <Space className="mb-3">
            {isDeleteVisible && (
              <Popconfirm
                onConfirm={handleDeleteMultiple}
                title={t('UserManagement_Header_#Are you sure?')}
                okText={t('UserManagement_Header_#Yes')}
                cancelText={t('UserManagement_Header_#No')}
              >
                <Button className="shadow-sm" type="primary" danger icon={<DeleteOutlined />} />
              </Popconfirm>
            )}

            <Button
              onClick={() => handleToggleAddOrUpdateShowUserModal()}
              className="shadow-sm"
              type="default"
              icon={<PlusOutlined />}
            />

            <Button
              className="shadow-sm"
              onClick={handleExportData}
              loading={store.UserManagementUtilities.isDownloading}
              type="default"
              icon={<DownloadOutlined />}
            />
            {/* Testing button for the PDF */}
            {/* <Button
              className="shadow-sm"
              onClick={handleExportPdf}
              loading={store.UserManagementUtilities.isDownloadingPdf}
              type="default"
              icon={<CopyOutlined />}
            /> */}
            <Input.Search
              className="shadow-sm"
              onSearch={handleUserSearch}
              onChange={handleOnChangeSearch}
              id="exact-search"
              placeholder={t('UserManagement_Header_#Search')}
            />
          </Space>
        </Col>
      </Row>

      <Suspense fallback={FallBackLoaders.EmptyLoader}>
        <AddOrUpdateUserModal form={form} />
      </Suspense>
    </>
  );
}

export default inject('store')(observer(UserManagementHeader));
