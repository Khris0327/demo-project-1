import { message } from 'antd';

const UserManagementController = ({ store, form, t, setIsDeleteVisible }) => {
  const getUsers = async (searchValue = null) => {
    const page = 1;

    const search = searchValue !== null ? searchValue : store.UserManagementUtilities.search;
    const props = store.UserManagementUtilities.props;
    let params = { page, search, props };

    await store.users.LIST(params);
  };

  const onChangePage = async (page) => {
    const search = store.UserManagementUtilities.search;
    const props = store.UserManagementUtilities.props;
    store.UserManagementUtilities.setChangePage(page);

    let params = { page, search, props };

    await store.users.LIST(params);
  };

  const handleToggleAddOrUpdateShowUserModal = (isUpdate = false, userData = null) => {
    form.resetFields();
    if (isUpdate) {
      form.setFieldsValue(userData);
      store.UserManagementUtilities.setUpdateId(userData?.id);
    }

    store.UserManagementUtilities.setToggleShowAddOrUpdateUserModal(isUpdate);
  };

  const handleDeleteMultiple = async () => {
    const selectedRowIds = store.UserManagementUtilities.selectedRowsKeys;
    const currentPage = store.UserManagementUtilities.currentPage;
    await store.users.DELETE(selectedRowIds);
    setIsDeleteVisible(false);
    await onChangePage(currentPage);
  };

  const handleSetSelectedRowKeys = async (setSelectedRowsKeys) => {
    store.UserManagementUtilities.setSelectedRowsKeys(setSelectedRowsKeys);
  };

  const handleUpdateOrCreateUser = async (values, isUpdate, isSelf = false) => {
    const newVal = { ...values, lang: store.globalState.language };
    message.loading({
      content: isUpdate
        ? t('UserManagement_UpdateModal_#Updating User')
        : t('UserManagement_AddModal_#Creating user'),
      key: 'creatingUser'
    });

    const params = isUpdate
      ? [isSelf ? store.login.id : store.UserManagementUtilities.updateId, values] // If updating self use id in login store
      : [newVal];
    let [successMessage, error] = await store.users[isUpdate ? 'UPDATE' : 'CREATE'](...params);

    if (!isUpdate) {
      store.users.setTotal(store.users.total + 1);
    }

    let success = await _showresultMessage(error, {
      successMessage: t(successMessage?.message) // isUpdate ? t('User Updated') :
    });

    if (success) {
      if (!isSelf) {
        form.resetFields();
        store.UserManagementUtilities.setToggleShowAddOrUpdateUserModal();
      }
    }
  };

  const handleFetchUserTypes = async () => {
    await store.UserManagementUtilities.FETCH_USER_TYPES();
  };

  const handleOnChangeSearch = async (search) => {
    let value = search.target.value;
    if (!value) {
      await getUsers(value);
    }
  };

  const handleUserSearch = async (search) => {
    search = search.trim();
    const props = store.UserManagementUtilities.props;
    let params = { search, props };

    store.UserManagementUtilities.setSearch(search);
    await store.users.LIST(params);
  };

  const handleChangeUserStatus = async (params) => {
    let { userData, active } = params;

    let [successMessage, error] = await store.users.UPDATE(userData.id, { active });

    await _showresultMessage(error, {
      successMessage: t(successMessage.message)
    });
  };

  const csrfProtect = async () => {
    await store.users.GET_CSRF_TOKEN();
  };

  const handleSendEmailToUser = async (userData) => {
    message.loading({
      content: t('UserManagement_#Sending Email'),
      key: 'creatingUser'
    });

    let [successResponse, error] = await store.users.SEND_EMAIL(
      {
        id: userData.id,
        email: userData.email
      },
      store.globalState.language
    );

    await _showresultMessage(error, {
      successMessage: t(successResponse.message)
    });
  };

  async function _showresultMessage(error, { successMessage }) {
    return new Promise((resolve, reject) => {
      if (error) {
        message.error({ content: t(error.response.data.error.message), key: 'creatingUser' });

        reject(false);
      } else {
        message.success({ content: successMessage, key: 'creatingUser' });
        resolve(true);
      }
    });
  }

  const deleteFile = async () => {
    store.users.deleteFile();
  };
  const handleExportData = async () => {
    store.UserManagementUtilities.setIsDownloading(true);

    let response = await store.users.exportData();

    if (response.data) {
      //let response = await store.feedback.getCSV();
      response.data = _changeCSVSeparator(response.data);
      //const url = window.URL.createObjectURL(new Blob([response.data]));
      const url = window.URL.createObjectURL(
        new Blob(['\ufeff', response.data], {
          type: response.headers['content-type'] + ';text/csv;charset=utf-8'
        })
      );
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `export_file.csv`); //or any other extension
      document.body.appendChild(link);
      link.click();
      await deleteFile();
    }
  };

  //PDF generation function
  const handleExportPdf = async () => {
    store.UserManagementUtilities.setIsDownloadingPdf(true);

    let response = await store.users.pdfData();

    if (response.data) {
      const url = window.URL.createObjectURL(
        new Blob([response.data], { type: 'application/pdf' })
      );
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `Output.pdf`); //or any other extension
      document.body.appendChild(link);
      link.click();
    }
    store.UserManagementUtilities.setIsDownloadingPdf(false);
  };

  function _changeCSVSeparator(data) {
    let locale = store.globalState.language;

    switch (locale) {
      case 'de':
        //console.log('data', data);
        return data.replace(/,/g, ';');
      default:
        return data;
    }
  }
  return {
    getUsers,
    handleExportData,
    onChangePage,
    handleToggleAddOrUpdateShowUserModal,
    handleUpdateOrCreateUser,
    handleFetchUserTypes,
    handleUserSearch,
    handleOnChangeSearch,
    handleChangeUserStatus,
    handleSendEmailToUser,
    handleDeleteMultiple,
    handleSetSelectedRowKeys,
    csrfProtect,
    handleExportPdf
  };
};

export default UserManagementController;
