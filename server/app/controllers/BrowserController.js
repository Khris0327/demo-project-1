const fs = require('fs');
const path = require('path');

const BaseController = require('./base/BaseController');

class BrowserController extends BaseController {
  constructor(db) {
    super(db);
    this.db = db;
  }

  getVersions = async (payload, [req, res]) => {
    const browserVersionsPath = path.resolve('app', 'data');
    const filename = 'latestBrowserVersions.json';
    const data = fs.readFileSync(path.join(browserVersionsPath, filename), 'utf-8');

    res.status(200).json({ data: JSON.parse(data) });
  };
}

module.exports = (db) => new BrowserController(db);
