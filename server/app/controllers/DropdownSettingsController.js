const BaseController = require('./base/BaseController');
const { Upsert } = require('../../utils');
const ObjectsToCsv = require('objects-to-csv');
const path = require('path');
const fs = require('fs');
const csv = require('csv-parser');
const { Duplex } = require('stream');
const iconv = require('iconv-lite');
class DropdownSettingsController extends BaseController {
  constructor(db) {
    super(db.EF_DropdownSetting);
    this.db = db;
    this.csvPath = path.resolve('app', 'dropdown_csv_file', 'dropdown_csv.csv');
  }

  generateCsv = async (payload, [req, res]) => {
    try {
      let response = await this.model.findAll();
      let data = this._cleanCSVData(response);

      const csv = new ObjectsToCsv(data).toString();
      return csv;
    } catch (error) {
      return false;
    }
  };

  list = async (_, [req]) => {
    let { dropdown_type } = req.query;

    let extraWhere = dropdown_type ? { dropdown_type } : {};

    let response = await this.model.findAll({ where: { ...extraWhere, status: 1 } });
    return { data: response };
  };

  deleteCSV = async (payload, [req, res]) => {
    try {
      fs.unlinkSync(this.csvPath);

      return true;
    } catch (error) {
      return false;
    }
  };

  _cleanCSVData = (data) => {
    return data.map((d) => {
      let { createdAt, updatedAt, ...rest } = d.dataValues;
      return { ...rest, status: rest.status ? 1 : 0 };
    });
  };

  create = async (payload, [req, res]) => {
    let bufferData = req.files.file.data;
    const GERMAN_CSV_TYPE_STRING = 'id;dropdown_type;entry_us;entry_de;value;status';
    let separator = ',';

    if (bufferData.toString().includes(GERMAN_CSV_TYPE_STRING)) {
      separator = ';';
    }

    const myReadableStream = this._bufferToStream(bufferData);
    let data = [];

    return new Promise((resolve, reject) => {
      myReadableStream
        .pipe(iconv.decodeStream('utf8'))
        .pipe(csv({ separator }))
        .on('data', (row) => {
          if (row.id === '' || isNaN(row.id)) {
            row.id = null;
          }
          data.push(row);
        })
        .on('error', (err) => {
          console.log(error);
          reject(err);
        })
        .on('end', async () => {
          let { rows, data: fixedData, valueIsNonNumeric } = this._checkIfValueIsNumber(data);
          let { statusIsNonNumeric } = this._checkIfStatusIsNumber(data);

          console.log(statusIsNonNumeric);

          if (valueIsNonNumeric || statusIsNonNumeric) {
            resolve({
              success: false,
              msg: `DropdownSetting_#Please do not insert any non-numeric values/status in value/status column`
            });
          } else {
            if (this._valueIsNumberButFormated(rows)) {
              resolve({
                success: false,
                msg: `DropdownSetting_#Please do not format any number, just add a decimal separator. Affected rows ${rows}`
              });
            } else {
              await Upsert.init(this.model, fixedData, {
                options: {
                  updateOnDuplicate: ['dropdown_type', 'entry_us', 'entry_de', 'value', 'status']
                }
              });
              resolve({
                success: true,
                msg: `DropdownSetting_#Succesfully updated dropdown settings data`
              });
            }
          }
        });
    });

    //return 'YES';
  };

  _valueIsNumberButFormated = (rows) => {
    return rows.length > 0;
  };

  _checkIfValueIsNumber = (data) => {
    //console.log(data);

    let rows = [];

    let index = 0;
    let rowNum = 1;
    let valueIsNonNumeric = false;

    for (let d of data) {
      let value = d.value;
      //  console.log(this._isNumber(value), 'HOY');

      if (this._isNumber(value)) {
        let isNumberFormated = this._isNumberFormated(value);

        if (isNumberFormated) {
          rows.push(rowNum);
        } else {
          data[index] = { ...data[index], value: parseFloat(data[index].value.replace(/,/, '.')).toFixed(2) };
        }
      } else if (value === '') {
        valueIsNonNumeric = false;
      } else {
        // rows.push(rowNum);
        valueIsNonNumeric = true;
      }
      index++;
      rowNum++;
    }

    return { rows, data, valueIsNonNumeric };
  };
  _checkIfStatusIsNumber = (data) => {
    //console.log(data);

    let rows = [];

    let index = 0;
    let rowNum = 1;
    let statusIsNonNumeric = false;

    for (let d of data) {
      let status = d.status;
      //  console.log(this._isNumber(status), 'HOY');

      if (this._isNumber(status)) {
        let isNumberFormated = this._isNumberFormated(status);

        if (isNumberFormated) {
          rows.push(rowNum);
        } else {
          data[index] = { ...data[index], status: parseFloat(data[index].status.replace(/,/, '.')).toFixed(2) };
        }
      } else if (status === '') {
        statusIsNonNumeric = false;
      } else {
        // rows.push(rowNum);
        statusIsNonNumeric = true;
      }
      index++;
      rowNum++;
    }

    return { rows, data, statusIsNonNumeric };
  };

  _isNumber = (value) => {
    const DEFAULT_NUMBER_STRING = '0.00';
    return value === DEFAULT_NUMBER_STRING || (/^[0-9,'.]*$/.test(value) && value !== '');
  };

  _isNumberFormated = (value) => {
    let occurances = this._countNumberOfOccurance(value);

    if (occurances > 1) {
      //number is formated
      return true;
    } else {
      //number is not formated
      return false;
    }
  };

  _isCommaSeparator = (value) => {};

  _bufferToStream = (myBuuffer) => {
    let tmp = new Duplex();
    tmp.push(myBuuffer);
    tmp.push(null);
    return tmp;
  };

  _countNumberOfOccurance = (string) => {
    let occurances = 0;
    if (isNaN(string)) {
      string = string.trim();
    }

    for (let i = 0; i <= string.length - 1; i++) {
      if (isNaN(string[i])) {
        occurances++;
      }
    }

    return occurances;
  };

  parsePotentiallyGroupedFloat = (stringValue) => {
    stringValue = stringValue.trim();
    var result = stringValue.replace(/[^0-9]/g, '');
    if (/[,\.]\d{2}$/.test(stringValue)) {
      result = result.replace(/(\d{2})$/, '.$1');
    }
    return parseFloat(result);
  };
}

module.exports = (db) => new DropdownSettingsController(db);
