const BaseController = require('./base/BaseController');
const { Upsert } = require('../../utils');
const ObjectsToCsv = require('objects-to-csv');
const path = require('path');
const fs = require('fs');

class DropdownSettingsControllerOLD extends BaseController {
  constructor(db) {
    super(db.DropdownSetting);
    this.db = db;

    this.csvPath = path.resolve('app', 'dropdown_csv_file', 'dropdown_csv.csv');
  }

  generateCsv = async (payload, [req, res]) => {
    try {
      let response = await this.model.findAll();
      let data = this.cleanCSVData(response);

      const csv = new ObjectsToCsv(data);

      // Save to file:
      await csv.toDisk(this.csvPath);

      return true;
    } catch (error) {
      return false;
    }
  };

  deleteCSV = async (payload, [req, res]) => {
    try {
      fs.unlinkSync(this.csvPath);

      return true;
    } catch (error) {
      return false;
    }
  };

  cleanCSVData = (data) => {
    return data.map((d) => {
      let { createdAt, updatedAt, ...rest } = d.dataValues;
      return { ...rest, status: rest.status ? 1 : 0 };
    });
  };

  create = async (payload, [req, res]) => {
    let bufferData = req.files.file.data;
    let stringData = bufferData.toString('utf8');

    let stringDataArray = stringData.split('\n');
    let data = this.cleanData(stringDataArray);

    await Upsert.init(this.model, data, {
      options: {
        updateOnDuplicate: ['dropdown_type', 'entry', 'value', 'status']
      }
    });

    return 'test';
  };

  list = async (_, [req]) => {
    let { dropdown_type } = req.query;

    let response = this.model.findAll({ where: { dropdown_type, status: 1 } });

    return response;
  };

  cleanData = (data) => {
    let isUpdate = this.isUpdate(data);
    data = this.reamoveItem(0, data);
    data = this.removeExtraString(data);
    data = this.removeEmptyItem(data);
    data = this.makeProperObject(data, isUpdate);

    return data;
  };

  isUpdate = (data) => {
    let firstItem = data.find((_, idx) => idx === 0);

    firstItem = firstItem.split(',')[0];

    return firstItem === 'id';
  };

  reamoveItem = (index, data) => {
    return data.filter((_, idx) => idx !== index);
  };

  removeExtraString = (data) => {
    return data.map((d) => {
      return d.replace('\r', '');
    });
  };

  removeEmptyItem = (data) => {
    return data.filter((c) => c !== '');
  };

  makeProperObject = (data, isUpdate) => {
    return data.map((d) => {
      let id, dropdown_type, entry, value, status;

      if (isUpdate) {
        id = parseInt(d.split(',')[0]);
        dropdown_type = d.split(',')[1];
        entry = d.split(',')[2];
        value = d.split(',')[3];
        status = d.split(',')[4];
        return { id, dropdown_type, entry, value, status };
      } else {
        dropdown_type = d.split(',')[0];
        entry = d.split(',')[1];
        value = d.split(',')[2];
        status = d.split(',')[3];
        return { dropdown_type, entry, value, status };
      }
    });
  };
}

module.exports = (db) => new DropdownSettingsControllerOLD(db);
