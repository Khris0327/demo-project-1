const BaseController = require('./base/BaseController');
const { Export } = require('../../utils');

class ExportController extends BaseController {
  constructor(db) {
    super(db);
    this.db = db;
  }

  toCSV = async (params, [req, res]) => {
    let { model, attributes } = req.query;

    const Users = await this.db[model].findAll({ raw: true, attributes });

    let csv = Export.generateCSV(Users);

    res.attachment('ExcelFile.csv');
    res.status(200).send(csv);
  };
}

module.exports = (db) => new ExportController(db);
