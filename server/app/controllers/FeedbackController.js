const BaseController = require('./base/BaseController');
const ObjectsToCsv = require('objects-to-csv');
const dayjs = require('dayjs');
const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');
const { Export } = require('../../utils');

const includes = (db) => ({
  include: [
    // {
    //   association: db.Feedback.UserRegisteredBy,
    //   attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] }
    // },
    {
      association: db.Feedback.UserHandlingBy,
      attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] }
    }
  ]
});

class FeedbackController extends BaseController {
  constructor(db) {
    super(db.Feedback, includes(db));
    this.db = db;
    this.csvPath = path.resolve('app', 'export_csv_file', 'export_file.csv');
  }

  exportCSV = async (params, [req, res]) => {
    let { attributes } = req.query;

    const Feedbacks = await this.model.findAll({
      attributes,
      include: [
        // {
        //   association: this.db.Feedback.UserRegisteredBy,
        //   attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] }
        // },
        {
          association: this.db.Feedback.UserHandlingBy,
          attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] }
        }
      ]
    });

    try {
      const data = Feedbacks.map((data) => {
        let obj = {
          ...data.dataValues,
          feedback_date: dayjs(data.feedback_date).format('DD/MM/YYYY'),
          event_date: dayjs(data.event_date).format('DD/MM/YYYY'),
          createdAt: dayjs(new Date(data.createdAt)).format('DD/MM/YYYY'),
          updatedAt: dayjs(new Date(data.updatedAt)).format('DD/MM/YYYY'),
          handling_by: data.UserHandlingBy ? `${data.UserHandlingBy.first_name} ${data.UserHandlingBy.last_name}` : ''
          //registered_by: data.UserRegisteredBy
          //  ? `${data.UserRegisteredBy.first_name} ${data.UserRegisteredBy.last_name}`
          //  : ''
        };

        delete obj.UserRegisteredBy;
        delete obj.UserHandlingBy;
        return obj;
      });
      // console.log('data', data);
      const csv = new ObjectsToCsv(data).toString();
      //console.log('csv', csv);
      // console.log('csvpath', this.csvPath);
      // Save to file:
      // await csv.toDisk(this.csvPath, { bom: true });

      return csv;
    } catch (error) {
      console.log('error', error);
      return error;
    }
  };

  deleteCSV = async (payload, [req, res]) => {
    try {
      //fs.unlinkSync(this.csvPath);

      return true;
    } catch (error) {
      return false;
    }
  };

  test = async () => {
    let result = await this.model.findAll(this.includes);
    return result;
  };
}

module.exports = (db) => new FeedbackController(db);
