const fs = require('fs');
const path = require('path');
const BaseController = require('./base/BaseController');
const createPdf = require('../../utils/PDFGenerator/create-pdf');
const renderTemplate = require('../../utils/PDFGenerator/render-template');
const dayjs = require('dayjs');

class PDFController extends BaseController {
  constructor(db) {
    super(db);
    this.db = db;
  }

  toPDF = async (params, [req, res]) => {
    let header = fs.readFileSync(path.resolve('app', 'pdf', 'views/partials/header.hbs'), 'utf-8');
    // let footer = fs.readFileSync(path.resolve('app', 'pdf', 'views/partials/footer.hbs'), 'utf-8');
    let body = fs.readFileSync(path.resolve('app', 'pdf', 'views/templates/TagesabrechnungLocalUnits.hbs'), 'utf-8');
    let contentHtml = renderTemplate('Sample PDF', { value: 'Sample Value' }, body);

    let pdf = createPdf(contentHtml, {
      path: `${__dirname}/../pdf/output/Output.pdf`,
      format: 'A4',
      displayHeaderFooter: true,
      headerTemplate: header,
      footerTemplate: `<span id="date" style=" color:#808080; font-size: 10px; width: 100%; height: 50px; background-color: red; border-top:1px solid #0ea6a7; margin-left:15px; margin-right:15px; padding: 5px;">
                            <span style="margin-left:10px;">Report Date [${dayjs().format('DD.MM.YY')}]</span>
                            <div style="float:right"><span class="pageNumber" ></span>/<span class="totalPages"></span></div>
                       </span>`
    });

    return pdf;
  };
}

module.exports = (db) => new PDFController(db);
