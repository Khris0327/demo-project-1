const path = require('path');
const ObjectsToCsv = require('objects-to-csv');
const dayjs = require('dayjs');
const bcrypt = require('bcryptjs');
const { Upsert, Mailer, ExistingHandler, ErrorException } = require(path.resolve('utils'));
const BaseController = require('./base/BaseController');
const AuthenticateController = require('./AuthenticateController');
const { exit } = require('process');

const includes = (db) => ({
  attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] },
  include: [{ association: db.EF_Users.Type }]
});

class UserController extends BaseController {
  constructor(db) {
    super(db.EF_Users, includes(db));
    this.db = db;
    this.auth = AuthenticateController(db);
  }

  exportCSV = async (params, [req, res]) => {
    let { attributes } = req.query;

    const Users = await this.model.findAll({
      attributes,
      include: [
        {
          association: this.db.EF_Users.Type,
          attributes: { exclude: ['password', 'isEmailAuthenticate', 'reset_password_token', 'verification_token'] }
        }
      ]
    });

    try {
      const data = Users.map((data) => {
        let obj = {
          user_type: data.Type.name,
          ...data.dataValues,
          createdAt: dayjs(new Date(data.createdAt)).format('DD/MM/YYYY'),
          updatedAt: dayjs(new Date(data.updatedAt)).format('DD/MM/YYYY')
        };
        delete obj.user_type_id;
        delete obj.Type;
        delete obj.verification_token;
        delete obj.reset_password_token;
        delete obj.isEmailAuthenticate;
        delete obj.password;

        return obj;
      });

      // console.log(data, ' Test');
      // console.log('data', data);
      const csv = new ObjectsToCsv(data).toString();
      //console.log('csv', csv);
      // console.log('csvpath', this.csvPath);
      // Save to file:
      // await csv.toDisk(this.csvPath, { bom: true });

      return csv;
    } catch (error) {
      console.log('error', error);
      return error;
    }
  };

  deleteCSV = async (payload, [req, res]) => {
    try {
      //fs.unlinkSync(this.csvPath);

      return true;
    } catch (error) {
      return false;
    }
  };

  test = async () => {
    return 'test';
  };

  showTypes = async () => {
    return await this.db.EF_UserTypes.findAll();
  };

  getUserBusinessUnits = async (user_id) => {
    return await this.db.UserBusinessUnits.findAll({
      attributes: ['business_unit_id'],
      where: { user_id },
      raw: true
    }).then((res) => res.map((e) => e.business_unit_id));
  };

  // BusinessUnits property should be present if user type is Standard User
  create = async (payload, [req]) => {
    // Check if email exists
    const [err] = await ExistingHandler.init(this.model, { email: payload.email });
    if (err) throw err;

    // If user type is a standard user, UserProperties is required
    // const opt = payload.user_type_id == 2 ? { include: [{ association: db.EF_Users.UserProperties }] } : {};

    // Create User
    const data = await Upsert.init(this.model, payload, { includes: this.includes });

    // Send Invitation email if checked
    if (payload.sendInviteEmail) await this.sendEmail({ payload: data, lang: payload.lang }, [req], true);

    return data;
  };

  update = async ({ id }, payload, [req]) => {
    const user_tmp = await this.model.findOne({ where: { id } });
    if (!user_tmp) throw new ErrorException.Error(404, "User ID doesn't exist!");

    // Check if email exists (changed email)
    const [err] = await ExistingHandler.init(this.model, { email: payload.email }, { id });
    if (err) throw err;

    // Delete password property if it's empty
    if ('password' in payload) if (payload.password.trim() === '') delete payload.password;

    // if (!payload.isSelf && !payload.hasOwnProperty('active')) {
    // 	// Create bulk for user property
    // 	await db.UserProperty.destroy({ where: { user_id: id } }).then(async (data) => {
    // 		return await db.UserProperty.bulkCreate(payload.UserProperties);
    // 	});
    // }

    return await this.updateUser({ payload }, id, req);
  };

  changePassword = async ({ id }, payload, [req]) => {
    const user_tmp = await this.model.findOne({ where: { id } });
    if (!user_tmp) throw new ErrorException.Error(404, "User ID doesn't exist!");

    // Check required fields before validating device
    this.auth.checkRequiredFields(payload, this.auth.REQUIRED_FIELDS.change_password);
    await this.auth.verifyUserResetPasswordToken(user_tmp, payload.token);
    await this.auth.authenticateResetPasswordToken({ token: payload.token, user_id: id });

    // Remove token from user
    payload.reset_password_token = null;

    const user = await Upsert.init(this.model, Object.assign(payload, { id }), { includes: this.includes });

    const data = await this.auth.validateDevice(payload, user, req.session);

    await this.db.EF_UserFingerprints.create({ user_id: id, fingerprint: req.body.fingerprint });

    return data;
  };

  changePassword2 = async (payload, [req]) => {
    //await this.db.UserFingerprints.create({ user_id: user.id, fingerprint: body.fingerprint });
    const decode = await this.auth.verifyToken(req.session.xdt, { subject: this.auth.subjects.SESSION_AUTH });

    const user = await this.model.findOne({ where: { id: decode.user.id } });
    if (!user) throw new ErrorException.Error(404, "User doesn't exist!");

    this.auth.checkRequiredFields(payload, this.auth.REQUIRED_FIELDS.change_password_2);

    const isMatch = await bcrypt.compare(payload.password, user.password);
    if (!isMatch) throw new ErrorException.Error(400, 'AccountSetting_#Password is incorrect');

    user.password = payload.new_password;
    await user.save();

    return this.response('AccountSetting_#Password successfully changed!', 'change_password_success');
  };

  updateUser = async (body, id, req) => {
    let { payload, lang } = body;

    // Update user
    const user = await Upsert.init(this.model, Object.assign(payload, { id }), { includes: this.includes });

    // If user update is self then resend token
    if (payload.isSelf || payload.isResetPassword) {
      const rememberMe = typeof payload.rememberMe !== 'undefined' ? payload.rememberMe : false;

      const payload_token = this.auth.payload(user, payload.rememberMe, payload.fingerprint);

      await this.auth.createToken(payload_token, { rememberMe, subject: this.auth.subjects.SESSION_AUTH }, req.session);

      return {
        user: payload_token.user,
        passwordchanged: !!payload.password
      };
    }

    // Send Invitation email if checked, this is only for normal user update from admin
    if (payload.sendInviteEmail && !payload.isResetPassword && !payload.isSelf)
      Mailer.SendAccount(payload.email, payload, lang);

    return user;
  };

  sendEmail = async (body, [req], isFromCreate) => {
    let { payload, lang } = body;
    let user;
    if (!isFromCreate) user = await this.model.findOne({ where: { id: payload.id } });
    else user = payload;

    // Create a token that lasts only for 2 days
    const token = this.auth.createToken(
      isFromCreate ? { ...payload.dataValues } : payload,
      { rememberMe: '2d', subject: this.auth.subjects.RESET_PASSWORD, audience: user.id },
      req.session,
      false
    );

    // Save token for user
    user.reset_password_token = token.split('.')[1];

    await user.save();
    payload = { ...user.dataValues, user_id: payload.id, token };

    // Send email to newly created user
    Mailer.SendAccount(user.email, payload, lang);

    return this.response('UserManagement_Table_#Email successully sent!', 'email_successfully_sent');
  };
}

module.exports = (db) => new UserController(db);
