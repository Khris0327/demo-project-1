const path = require('path');
const fs = require('fs');
const caniuse = require('caniuse-api');
const exec = require('child_process').exec;
const { MKDIRNotExist, CurrentDate } = require('../../utils');
const { NODE_ENV } = require('../../config/environment');
const BaseJob = require('./base');

const browserVersionsPath = path.resolve('app', 'data');

module.exports = class BrowserVersionCheck extends BaseJob {
  name = 'Browser Version Check';
  jobs = [];

  constructor(time = { daily: true, weekly: false, monthly: false, yearly: false }) {
    super();
    this.time = Object.entries(time).reduce((a, b) => a.concat(b[1] ? b[0] : []), []);

    MKDIRNotExist(browserVersionsPath);

    console.log(`📀 Initializing ${this.name} job. . .`);
    // initialize job backups
    this.time.forEach((t) => {
      this.executeBrowserVersionCheck(t);
    });
  }

  executeBrowserVersionCheck(time) {
    const dirPath = browserVersionsPath;
    const filename = 'latestBrowserVersions.json';
    const browserList = ['chrome', 'edge', 'firefox', 'opera', 'safari'];

    MKDIRNotExist(dirPath);

    this[time](() => {
      const allBrowsers = caniuse.getLatestStableBrowsers();
      const versions = allBrowsers.reduce((memo, bro) => {
        const parts = bro.split(' ');
        memo[parts[0]] = parts[1];
        return memo;
      }, {});

      const browserVersions = Object.keys(versions)
        .filter((key) => browserList.includes(key))
        .reduce((obj, key) => {
          obj[key] = versions[key];
          return obj;
        }, {});
      fs.writeFile(`${browserVersionsPath}/${filename}`, JSON.stringify(browserVersions), function (err) {
        if (err) console.log(err);
      });
    });

    console.log(`✅ (${time}) \t ${this.name}`);
  }

  start() {
    if (this.time.length) {
      console.log(`🚀 ${this.name} [Running]`);
      this.jobs.forEach((job) => job.start());

      return;
    }

    console.log('No backups to run.');
  }
};
