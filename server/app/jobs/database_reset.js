const exec = require('child_process').exec;
const BaseJob = require('./base');

module.exports = class DatabaseReset extends BaseJob {
  name = 'Database Reset';
  jobs = [];

  constructor(time = { daily: true, weekly: false, monthly: false, yearly: false }) {
    super();
    this.time = Object.entries(time).reduce((a, b) => a.concat(b[1] ? b[0] : []), []);

    console.log(`📀 Initializing ${this.name} jobs. . .`);
    // initialize job backups
    this.time.forEach((t) => {
      this.executeReset(t);
    });
  }

  executeReset(time) {
    this[time](() => {
      try {
        console.log('Executing database reset job...');
        exec('npm run db-flush');
      } catch (error) {
        console.log(error);
      }
    });

    console.log(`✅ (${time}) \t ${this.name}`);
  }

  start() {
    if (this.time.length) {
      console.log(`🚀 ${this.name} [Running]`);
      this.jobs.forEach((job) => job.start());

      return;
    }

    console.log('No reset jobs to run.');
  }
};
