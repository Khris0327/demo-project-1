module.exports = {
  us: (data) => {
    return {
      subject: `Device registration`,
      intro: `Dear ${data.user.first_name} ${data.user.last_name},`,
      content: `
        <p>This is your One-Time-Pin: ${data.verification_token}</p>
      `
    };
  },
  de: (data) => {
    return {
      subject: `Geräteregistrierung`,
      intro: `Guten Tag ${data.user.first_name} ${data.user.last_name},`,
      content: `
        <p>Dies ist Ihr einmaliger Pin: ${data.verification_token}</p>
      `
    };
  }
};
