const { APP_NAME, HOST } = require('../../../../config/environment');

module.exports = {
  us: (data) => {
    const reset_link = `${HOST}/change-password/${data.token}/${data.user_id}`;

    return {
      subject: `Reset password`,
      intro: `Dear ${data.first_name} ${data.last_name}`,
      content: `

			<p>You are receiving this email because you or someone else has requested a password reset for your user account at ${APP_NAME}.</p>

			<p>Please click the link below to reset your password:</p>
			<a class="primary-btn" href="${reset_link}">Reset password</a> <br />

			<p>This link is only valid for 2 days.  </p>

			<p>
        If you have not requested a password reset, please ignore this email.
			</p>
    `
    };
  },
  de: (data) => {
    const reset_link = `${HOST}/change-password/${data.token}/${data.user_id}`;

    return {
      subject: `Zurücksetzen des Passworts`,
      intro: `Guten Tag ${data.first_name} ${data.last_name}`,
      content: `
      
      <p>Sie erhalten dieses E-Mail, weil Sie oder jemand anderes ein Passwort-Reset für Ihr Benutzerkonto bei ${APP_NAME} beantragt hat.</p>

			<p>Klicken Sie auf den unten stehenden Link, um Ihr Passwort zurückzusetzen:</p>
			<a class="primary-btn" href="${reset_link}">Passwort zurücksetzen</a> <br />

			<p>Dieser Link ist nur für 2 Tage gültig.  </p>

			<p>
			  Wenn Sie keine Kennwortrücksetzung angefordert haben, können Sie dieses E-Mail ignorieren.
			</p>
    `
    };
  }
};
