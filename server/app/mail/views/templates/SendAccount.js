const { COMPANY_NAME, HOST, APP_NAME } = require('../../../../config/environment');

module.exports = {
  us: (data) => {
    const reset_link = `${HOST}/change-password/${data.token}/${data.user_id}`;

    return {
      subject: `Account registration`,
      intro: `Dear ${data.first_name} ${data.last_name}`,
      content: `
        <p>Your account for ${COMPANY_NAME} ${APP_NAME} has been created successfully.</p>
        <b>Login E-mail:</b> ${data.email} <br><br>  
        <a class="primary-btn" href="${reset_link}">Change password</a> <br />
  
        <p>This link is valid for 2 days.</p>
      `
    };
  },
  de: (data) => {
    const reset_link = `${HOST}/change-password/${data.token}/${data.user_id}`;

    return {
      subject: `Kontoregistrierung`,
      intro: `Guten Tag ${data.first_name} ${data.last_name}`,
      content: `
        <p>Ihr Konto für ${COMPANY_NAME} ${APP_NAME} wurde erfolgreich erstellt.</p>
        <b>Login E-mail:</b> ${data.email} <br><br>  
        <a class="primary-btn" href="${reset_link}">Passwort ändern</a> <br />
  
        <p>Dieser Link ist 2 Tage gültig.</p>
      `
    };
  }
};
