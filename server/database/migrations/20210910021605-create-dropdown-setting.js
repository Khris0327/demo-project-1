'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('EF_DropdownSettings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      dropdown_type: {
        type: Sequelize.STRING
      },
      entry_de: {
        type: Sequelize.STRING
      },
      entry_us: {
        type: Sequelize.STRING
      },
      value: {
        type: Sequelize.DECIMAL(65, 2)
      },
      status: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('EF_DropdownSettings');
  }
};
