'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Feedbacks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      //* FEEDBACK *//
      feedback_date: Sequelize.STRING,
      feedback_type: Sequelize.STRING,
      //* REPORTER *//
      order_number: Sequelize.STRING,
      company_name: Sequelize.STRING,
      last_name: Sequelize.STRING,
      position: Sequelize.STRING,
      street_no: Sequelize.STRING,
      zip_code: Sequelize.INTEGER,
      city: Sequelize.STRING,
      phone_number: Sequelize.STRING,
      email: Sequelize.STRING,
      //* FAILURE CLASS *//
      failure_class: Sequelize.STRING,
      failure_class_parent: Sequelize.STRING,
      //* ADDITIONAL DETAILS MODEL *//
      event_details: Sequelize.STRING,
      affected_employee: Sequelize.STRING,
      form_of_execution: Sequelize.STRING,
      handling_by: Sequelize.STRING,
      corrective_actions: Sequelize.STRING,
      status: Sequelize.STRING,
      // internal_measures: Sequelize.STRING,
      // reporter_category: Sequelize.STRING,
      // event_date: Sequelize.STRING,
      // event_time: Sequelize.STRING,
      // event_place: Sequelize.STRING,
      // registered_by: Sequelize.STRING,
      // address: Sequelize.STRING, //TO BE DECIDED
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Feedbacks');
  }
};
