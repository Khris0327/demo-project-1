'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EF_Locale extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  EF_Locale.init(
    {
      locale: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'EF_Locale'
    }
  );
  return EF_Locale;
};
