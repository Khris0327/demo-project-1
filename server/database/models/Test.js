'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Test extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(_) {}
  }
  Test.init(
    {
      test_id: DataTypes.INTEGER,
      another_id: DataTypes.INTEGER,
      desc: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'Test',
      freezeTableName: true,
      timestamps: false
    }
  );
  return Test;
};
