'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EF_UserFingerprints extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }
  EF_UserFingerprints.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT.UNSIGNED
      },
      user_id: DataTypes.BIGINT.UNSIGNED,
      fingerprint: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'EF_UserFingerprints'
    }
  );
  return EF_UserFingerprints;
};
