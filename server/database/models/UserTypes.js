'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EF_UserTypes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      EF_UserTypes.User = EF_UserTypes.hasMany(models.EF_Users, {
        as: 'Users',
        foreignKey: 'user_type_id'
      });
    }
  }
  EF_UserTypes.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      name: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'EF_UserTypes'
    }
  );
  return EF_UserTypes;
};
