'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class EF_DropdownSetting extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  EF_DropdownSetting.init(
    {
      dropdown_type: DataTypes.STRING,
      entry_us: DataTypes.STRING,
      entry_de: DataTypes.STRING,
      value: DataTypes.DECIMAL(65, 2),
      status: DataTypes.BOOLEAN
    },
    {
      sequelize,
      modelName: 'EF_DropdownSetting'
    }
  );
  return EF_DropdownSetting;
};
