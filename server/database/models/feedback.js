'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Feedback.UserRegisteredBy = Feedback.belongsTo(models.EF_Users, {
      //   as: 'UserRegisteredBy',
      //   foreignKey: 'registered_by'
      // });
      Feedback.UserHandlingBy = Feedback.belongsTo(models.EF_Users, {
        as: 'UserHandlingBy',
        foreignKey: 'handling_by'
      });
    }
  }
  Feedback.init(
    {
      //* FEEDBACK MODEL *//
      feedback_date: DataTypes.STRING,
      feedback_type: DataTypes.STRING,
      //* REPORTER MODEL *//
      order_number: DataTypes.STRING,
      company_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      position: DataTypes.STRING,
      street_no: DataTypes.STRING,
      zip_code: DataTypes.INTEGER,
      city: DataTypes.STRING,
      phone_number: { type: DataTypes.STRING, allowNull: true },
      email: { type: DataTypes.STRING, allowNull: true },
      //* FAILURE CLASS MODEL *//
      failure_class_parent: { type: DataTypes.STRING, allowNull: true },
      failure_class: { type: DataTypes.STRING, allowNull: true },
      //* ADDITIONAL DETAILS MODEL *//
      event_details: { type: DataTypes.STRING, allowNull: true },
      affected_employee: { type: DataTypes.STRING, allowNull: true },
      form_of_execution: { type: DataTypes.STRING, allowNull: true },
      handling_by: { type: DataTypes.STRING, allowNull: true },
      corrective_actions: { type: DataTypes.STRING, allowNull: true },
      status: DataTypes.STRING
      //* UNUSED MODEL *//
      // internal_measures: { type: DataTypes.STRING, allowNull: true },
      // reporter_category: { type: DataTypes.STRING, allowNull: true },
      // event_date: { type: DataTypes.STRING, allowNull: true },
      // event_time: { type: DataTypes.STRING, allowNull: true },
      // event_place: { type: DataTypes.STRING, allowNull: true },
      // registered_by: { type: DataTypes.STRING, allowNull: true }
    },
    {
      sequelize,
      modelName: 'Feedback'
    }
  );
  return Feedback;
};
