'use strict';

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert(
      'EF_UserTypes',
      [
        {
          name: 'Administrator'
        },
        {
          name: 'Standard User'
        }
      ],
      {}
    );
  },

  down: async () => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
