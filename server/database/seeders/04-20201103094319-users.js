('use strict');
const { v4: uuidv4 } = require('uuid');
const { faker } = require('@faker-js/faker');

faker.locale = 'de_CH';

const users = [...Array(18)].map((user) => ({
  id: uuidv4(),
  first_name: faker.name.firstName(),
  last_name: faker.name.lastName(),
  email: faker.internet.email(),
  password: faker.internet.password(8),
  user_type_id: 2,
  phone_number: faker.phone.phoneNumber(),
  active: true,
  isEmailAuthenticate: true
}));

users.push(
  {
    id: '7cef14d3-f91e-4134-bee9-e56f772b0e86',
    first_name: 'Test',
    last_name: 'User',
    email: 'test.user@exact-construct.ch',
    password: '$2a$12$WMw4qwvwfBc7Pjml4xAV5ejUKM/tKUlu6G6Ov/l3jBOJvDfodYoXq',
    user_type_id: 2,
    phone_number: faker.phone.phoneNumber(),
    active: true,
    isEmailAuthenticate: false
  },
  {
    id: 'bdd4cdd4-d0d5-42ca-ba24-f09cca0759a6',
    first_name: 'Admin ',
    last_name: 'User',
    email: 'admin@exact-construct.ch',
    password: '$2a$12$gYCrERUwJd4EVyE3.vVzGOevuyJiR/GfAOU/.mr.c822F3pMPnrIS',
    user_type_id: 1,
    phone_number: faker.phone.phoneNumber(),
    active: true,
    isEmailAuthenticate: false
  }
);

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('EF_Users', users, {});
  },

  down: async () => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
