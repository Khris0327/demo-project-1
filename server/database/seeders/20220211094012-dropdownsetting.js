'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'EF_DropdownSettings',

      [
        {
          dropdown_type: 'feedback_type',
          entry_us: 'Anerkennung',
          entry_de: 'Anerkennung',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'feedback_type',
          entry_us: 'Beschwerde',
          entry_de: 'Beschwerde',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'status',
          entry_us: 'in Bearbeitung',
          entry_de: 'in Bearbeitung',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'status',
          entry_us: 'abgeschlossen',
          entry_de: 'abgeschlossen',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'failure_class',
          entry_us: 'Verkauf',
          entry_de: 'Verkauf',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'failure_class',
          entry_us: 'Planung',
          entry_de: 'Planung',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'failure_class',
          entry_us: 'Produktion',
          entry_de: 'Produktion',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'failure_class',
          entry_us: 'Administration',
          entry_de: 'Administration',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Verkauf',
          entry_us: 'Kundendaten falsch erfasst',
          entry_de: 'Kundendaten falsch erfasst',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Verkauf',
          entry_us: 'Auftrag falsch erfasst',
          entry_de: 'Auftrag falsch erfasst',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Verkauf',
          entry_us: 'Unfreundliches Auftreten',
          entry_de: 'Unfreundliches Auftreten',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Verkauf',
          entry_us: 'Unzureichende Beratung',
          entry_de: 'Unzureichende Beratung',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Verkauf',
          entry_us: 'Termin nicht eingehalten',
          entry_de: 'Termin nicht eingehalten',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Planung',
          entry_us: 'Inkorrekte Masse eingeplant',
          entry_de: 'Inkorrekte Masse eingeplant',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Planung',
          entry_us: 'Falsches Material eingeplant',
          entry_de: 'Falsches Material eingeplant',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Planung',
          entry_us: 'Mitarbeiter nicht eingeplant',
          entry_de: 'Mitarbeiter nicht eingeplant',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Produktion',
          entry_us: 'Auftrag falsch erfasst',
          entry_de: 'Auftrag falsch erfasst',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Produktion',
          entry_us: 'Auftrag falsch ausgeführt',
          entry_de: 'Auftrag falsch ausgeführt',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Produktion',
          entry_us: 'Termin nicht eingehalten',
          entry_de: 'Termin nicht eingehalten',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Administration',
          entry_us: 'Rechnungsanschrift nicht korrekt',
          entry_de: 'Rechnungsanschrift nicht korrekt',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Administration',
          entry_us: 'Rechnungsbetrag nicht korrekt',
          entry_de: 'Rechnungsbetrag nicht korrekt',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'Administration',
          entry_us: 'Leistung inkorrekt verrechnet',
          entry_de: 'Leistung inkorrekt verrechnet',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'form_of_execution',
          entry_us: 'Erledigt ohne Rückmeldung',
          entry_de: 'Erledigt ohne Rückmeldung',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'form_of_execution',
          entry_us: 'Telefonische Erledigung mit Telefonnotiz',
          entry_de: 'Telefonische Erledigung mit Telefonnotiz',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'form_of_execution',
          entry_us: 'Schriftliche Antwort',
          entry_de: 'Schriftliche Antwort',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          dropdown_type: 'form_of_execution',
          entry_us: 'Entschuldigungsschreiben',
          entry_de: 'Entschuldigungsschreiben',
          value: '0',
          status: '1',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
