'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    //const today = `${CurrentDate.year}-${CurrentDate.month}-${CurrentDate.day}`;
    await queryInterface.bulkInsert(
      'Feedbacks',
      [
        {
          //* FEEDBACK *//
          feedback_date: '2021-07-19',
          feedback_type: 'Beschwerde',
          //* REPORTER *//
          order_number: '875849302827',
          company_name: 'Test AG',
          last_name: 'Lillian Ganz-Gross',
          position: 'Head Quality Manager',
          street_no: 'Hauptstrasse 123',
          zip_code: '9458',
          city: 'Altstätten',
          phone_number: '058 123 45 67',
          email: 'testag@exact-construct.ch',
          //* FAILURE CLASS *//
          failure_class: 'Auftrag falsch erfasst',
          failure_class_parent: 'Verkauf',
          //* ADDITIONAL DETAILS MODEL *//
          event_details: '',
          affected_employee: '',
          form_of_execution: '',
          handling_by: 'bdd4cdd4-d0d5-42ca-ba24-f09cca0759a6',
          corrective_actions: '',
          status: 'in Bearbeitung',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          //* FEEDBACK *//
          feedback_date: '2021-10-07',
          feedback_type: 'Beschwerde',
          //* REPORTER *//
          order_number: '486340046898',
          company_name: 'Beta GmbH',
          last_name: 'Donald Duck',
          position: 'CEO',
          street_no: 'Entenweg 1',
          zip_code: '4133',
          city: 'Entenhausen',
          phone_number: '0900 589 101',
          email: 'betagmbh@exact-construct.ch',
          //* FAILURE CLASS *//
          failure_class: 'Leistung inkorrekt verrechnet',
          failure_class_parent: 'Administration',
          //* ADDITIONAL DETAILS MODEL *//
          event_details: '',
          affected_employee: '',
          form_of_execution: 'Schriftliche Antwort',
          handling_by: '7cef14d3-f91e-4134-bee9-e56f772b0e86',
          corrective_actions: '',
          status: 'abgeschlossen',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          //* FEEDBACK *//
          feedback_date: '2022-01-10',
          feedback_type: 'Beschwerde',
          //* REPORTER *//
          order_number: '791782368316',
          company_name: 'Sommer & Winter',
          last_name: 'Hans Meier',
          position: 'Verkaufsleiter',
          street_no: 'Bergweg 66',
          zip_code: '8355',
          city: 'Bergen',
          phone_number: '+41 88 192 52 20',
          email: 'sw@exact-construct.ch',
          //* FAILURE CLASS *//
          failure_class: 'Kundendaten falsch erfasst',
          failure_class_parent: 'Verkauf',
          //* ADDITIONAL DETAILS MODEL *//
          event_details: '',
          affected_employee: '',
          form_of_execution: '',
          handling_by: '7cef14d3-f91e-4134-bee9-e56f772b0e86',
          corrective_actions: '',
          status: 'in Bearbeitung',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          //* FEEDBACK *//
          feedback_date: '2022-01-27',
          feedback_type: 'Beschwerde',
          //* REPORTER *//
          order_number: '858573393942',
          company_name: 'Testus AG',
          last_name: 'Peter Müller',
          position: 'Produktionsleiter',
          street_no: 'Teststrasse 22a',
          zip_code: '3380',
          city: 'Testingen',
          phone_number: '+41 12 123 45 67',
          email: 'testusag@exact-construct.ch',
          //* FAILURE CLASS *//
          failure_class: 'Termin nicht eingehalten',
          failure_class_parent: 'Verkauf',
          //* ADDITIONAL DETAILS MODEL *//
          event_details: '',
          affected_employee: '',
          form_of_execution: '',
          handling_by: 'bdd4cdd4-d0d5-42ca-ba24-f09cca0759a6',
          corrective_actions: '',
          status: 'in Bearbeitung',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          //* FEEDBACK *//
          feedback_date: '2022-03-16',
          feedback_type: 'Anerkennung',
          //* REPORTER *//
          order_number: '598321932026',
          company_name: 'Oster & Has',
          last_name: 'Oster Hase',
          position: 'CFO',
          street_no: 'Blumenweise 10',
          zip_code: '3541',
          city: 'Hasenhausen',
          phone_number: '011 111 11 11',
          email: 'cfo.oster@exact-construct.ch',
          //* FAILURE CLASS *//
          failure_class: '',
          failure_class_parent: '',
          //* ADDITIONAL DETAILS MODEL *//
          event_details: '',
          affected_employee: '',
          form_of_execution: 'Erledigt ohne Rückmeldung',
          handling_by: 'bdd4cdd4-d0d5-42ca-ba24-f09cca0759a6',
          corrective_actions: '',
          status: 'abgeschlossen',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
