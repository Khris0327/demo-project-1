const express = require('express');
const Server = require('./Server');
const { NODE_ENV, PORT, HOST } = require('./config/environment');

// Execute cron jobs
const DatabaseBackup = require('./app/jobs/database_backup');
const DatabaseReset = require('./app/jobs/database_reset');
const BrowserVersionCheck = require('./app/jobs/browser_version_check');

//if (NODE_ENV === 'production') {
// const DBBackup = new DatabaseBackup();
// try {
//   DBBackup.start();
// } catch (error) {
//   console.log(error);
// }
const BrowserCheck = new BrowserVersionCheck();
BrowserCheck.start();

const DBReset = new DatabaseReset();
DBReset.start();

//}

// Start Server
const server = new Server(express, PORT, NODE_ENV, HOST);

server.start();
