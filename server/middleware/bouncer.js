const path = require('path');
const { ErrorException } = require(path.resolve('utils'));

const minMs = 15 * 60 * 1000; // 15mins
const maxMs = 60 * 60 * 1000; // 60mins
const maxAttempts = 5; // blocked on the 6th attempt

const bouncer = require('express-bouncer')(minMs, maxMs, maxAttempts);

bouncer.blocked = function (req, res, next, remaining) {
  throw new ErrorException.Error(
    429,
    'Too many login attempts. Blocked for: ' + parseInt(remaining / 1000 / 60) + ' minute(s)'
  );
};

module.exports = bouncer;
