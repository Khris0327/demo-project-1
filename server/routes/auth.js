const { AuthenticateController } = require('../app/controllers');
const bouncer = require('../middleware/bouncer');
var csrf = require('csurf');

var csrfProtection = csrf({ cookie: true });

module.exports = (router, rw, auth) => {
  router.post('/', [csrfProtection, bouncer.block], rw(AuthenticateController.authenticateUser));

  router.post('/login_device', rw(AuthenticateController.loginDevice));

  router.get('/token', rw(AuthenticateController.authenticateSessionToken));

  router.post('/reset_password', rw(AuthenticateController.resetPassword));

  router.get('/reset_password/:token/:user_id', rw(AuthenticateController.authenticateResetPasswordToken));

  router.post('/logout', auth, rw(AuthenticateController.logout));

  return router;
};
