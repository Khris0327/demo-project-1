const { BrowserController } = require('../app/controllers');

module.exports = (router, rw, auth) => {
  router.applyRoutes(BrowserController, {
    '/': {
      default: true
    },
    '/getLatestVersion': {
      get: BrowserController.getVersions
    }
  });

  return router;
};
