const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.CRYPTR_SECRET_KEY);
// setup route middlewares
var csrf = require('csurf');

var csrfProtection = csrf({ cookie: true });

module.exports = (router, _, auth) => {
  router.get('/get_csrf_token', csrfProtection, function (req, res) {
    // pass the csrfToken to the view
    res.json({ csrfToken: req.csrfToken() }); //cryptr.encrypt(req.csrfToken())
  });

  return router;
};
