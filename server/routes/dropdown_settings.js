const { DropdownSettingsController } = require('../app/controllers');

module.exports = (router, rw, auth) => {
  router.applyRoutes(DropdownSettingsController, {
    '/': {
      default: true,
      middleware: auth
    },
    '/:id': {
      get: DropdownSettingsController.filter,
      middleware: auth
    },
    '/generate_csv': {
      get: DropdownSettingsController.generateCsv,
      middleware: auth
    },
    '/delete_csv': {
      delete: DropdownSettingsController.deleteCSV,
      middleware: auth
    }
  });

  return router;
};
