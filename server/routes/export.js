const { ExportController } = require('../app/controllers');

module.exports = (router, rw, auth) => {
  router.applyRoutes(ExportController, {
    '/': {
      get: ExportController.toCSV
      //middleware: auth
    }
  });

  return router;
};
