const { FeedbackController } = require('../app/controllers');

module.exports = (router, rw, auth) => {
  router.applyRoutes(FeedbackController, {
    '/': {
      default: true,
      middleware: auth
    },
    '/export_csv': {
      get: FeedbackController.exportCSV,
      middleware: auth
    },
    '/delete_csv': {
      delete: FeedbackController.deleteCSV
    }
  });

  return router;
};
