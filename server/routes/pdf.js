const { PDFController } = require('../app/controllers');

module.exports = (router, rw, auth) => {
  router.applyRoutes(PDFController, {
    '/': {
      get: PDFController.toPDF
      //middleware: auth
    }
  });

  return router;
};
