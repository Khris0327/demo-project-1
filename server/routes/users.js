const { UserController } = require('../app/controllers');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });

module.exports = (router, rw, auth) => {
  router.put('/change_password', rw(UserController.changePassword2));

  router.applyRoutes(UserController, {
    '/': {
      default: ['get', 'post', 'put', 'delete'],
      middleware: auth
    },
    '/export_csv': {
      get: UserController.exportCSV,
      middleware: auth
    },
    '/delete_csv': {
      delete: UserController.deleteCSV
    },
    '/change_password': {
      '/:id': {
        put: UserController.changePassword
      }
    },
    '/types': {
      get: UserController.showTypes,
      middleware: auth
    },
    '/:id': {
      get: UserController.show,
      middleware: auth
    },
    '/send_email': {
      post: UserController.sendEmail,
      middleware: auth
    },
    '/something': {
      get: UserController.test
    }
  });
  return router;
};
