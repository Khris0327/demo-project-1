const { describe, it } = require('mocha');
const expect = require('chai').expect;
const db = require('../database/models');
const { Upsert } = require('../utils');

describe('Bulk upsert', () => {
  it('should update duplicate keys on bulk update', async () => {
    // Initialize data
    const payload = [
      {
        test_id: 1,
        another_id: 1,
        desc: 'new value'
      },
      {
        test_id: 2,
        another_id: 2,
        desc: 'new value 5'
      },
      {
        test_id: 1,
        another_id: 2,
        desc: 'new value 2'
      }
    ];

    // Modify data
    await Upsert.init(db.Test, payload, {
      options: {
        updateOnDuplicate: {
          keys: ['test_id', 'another_id'],
          fields: ['desc']
        },
        logging: true
      }
    });
    const data = await db.Test.findAll({ raw: true });

    // Reset Data
    await truncateTest();
    await seedTest();

    // Test data
    expect(data.length).equals(3);
    expect(data.find((e) => e.test_id === 1 && e.another_id === 1).desc).equals('new value');
    expect(data.find((e) => e.test_id === 2 && e.another_id === 2).desc).equals('new value 5');
  }, 10000);
});

async function truncateTest() {
  await db.Test.destroy({
    where: {},
    truncate: true
  });
}

async function seedTest() {
  const payload = [
    {
      test_id: 1,
      another_id: 1,
      desc: 'test 1'
    },
    {
      test_id: 2,
      another_id: 2,
      desc: 'test 2'
    }
  ];

  await db.Test.bulkCreate(payload);
}
