const bcrypt = require('bcryptjs');
const { Parser } = require('json2csv');
module.exports = class Export {
  static generateCSV(data, res) {
    const fields = Object.keys(data[0] || {});
    const json2csv = new Parser({ fields });
    const csv = json2csv.parse(data);

    return csv;
  }

  static async hash(password) {
    try {
      const hash = await bcrypt.hash(password, 10);
      return [password, hash];
    } catch (err) {
      return [err];
    }
  }
};
