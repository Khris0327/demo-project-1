const path = require('path');
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const { combine, timestamp, printf } = format;
const jwt = require('jsonwebtoken');
const JWTController = require(path.resolve('app/controllers/auth/JWTController.js'));

//GET USER INFO
const JWTControl = new JWTController();
const decodeUser = async (token) => {
  return token ? await jwt.verify(token, JWTControl.keys.public) : '';
};
//USER CHECKING
const getUserData = async (token) => {
  const data = await decodeUser(token);
  return data !== ''
    ? `"Name: ${data.user.first_name} ${data.user.last_name}" | User Type: "${data.user.Type.name}"`
    : 'N/A';
};
//METHOD CHECKER
const checkMethod = (method) => {
  //GET METHOD API WILL NOT BE LOGGED
  if (method !== 'GET') return true;
};

//===== LOG-FILE MODULE ====//
//LOG FORMATTING
const logFormat = printf(({ level, message, timestamp }) => {
  return `[${timestamp}] ${level.toUpperCase()} | ${message}`;
});
//COMMON TRANSPORT SETUP
const transportSetup = { datePattern: 'YYYY-MM-DD', zippedArchive: true, maxSize: '50m', maxFiles: '60d' };
//ERROR TRANSPORT SETUP
const errorTransport = new transports.DailyRotateFile({
  filename: 'ef-error-%DATE%.log',
  dirname: '../server/logs/error/',
  ...transportSetup
});
//ACCESS TRANSPORT SETUP
const accessTransport = new transports.DailyRotateFile({
  filename: 'ef-access-%DATE%.log',
  dirname: '../server/logs/access/',
  ...transportSetup
});
//LOGGER INIT FOR ERRORS
const logger = createLogger({
  level: 'error',
  format: combine(timestamp({ format: `YYYY-MM-DD HH:mm:ss` }), logFormat),
  transports: [errorTransport]
});
//LOGGER INIT FOR ACCESS
const access = createLogger({
  level: 'info',
  format: combine(timestamp({ format: `YYYY-MM-DD HH:mm:ss` }), logFormat),
  transports: [accessTransport]
});
//LOG ERRORS
const logError = async (token, url, name, message, stack) => {
  const userInfo = await getUserData(token);
  logger.error(
    `${userInfo} | API Path: "${url}" | Name: "${name}" | Message: ${JSON.stringify(message)} | Stack: "${stack}"`
  );
};
//LOG ACCESS
const logAccess = async (token, url, method, sentData, receivedData) => {
  const userInfo = await getUserData(token);
  if (checkMethod(method))
    access.info(
      `${userInfo} | API Path: "${url}" | Sent Data: ${JSON.stringify(sentData)} | Received Data: ${JSON.stringify(
        receivedData
      )}`
    );
};

module.exports = { logError, logAccess };
