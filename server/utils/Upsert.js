const { Op } = require('sequelize');

module.exports = class Upsert {
  static async init(Model, values, { options, includes = {}, where }) {
    // Placed default value here instead on param level | Reason: On param level, it doesn't get the value 'values.id'
    where = where || { id: values.id || -1 };

    if (!Array.isArray(values)) {
      return await Model.findOne({ where, ...includes }).then(async function (obj) {
        if (options == null) options = {};

        if (obj) {
          // update
          const { fields, updateOnDuplicate, ...option } = options;
          await obj.update(values, { individualHooks: true, ...option });
          return await Model.findOne({ where, ...includes });
        } else {
          const { id, updateOnDuplicate, ...rest } = values;
          // insert
          // If include is not empty
          if (!!Object.keys(includes).length) {
            const result = await Model.create(rest, { ...options }).then((resultEntity) =>
              resultEntity.get({ plain: true })
            );
            return await Model.findOne({ where: { id: result.id }, ...includes });
          } else {
            return await Model.create(rest, { ...options });
          }
        }
      });
    } else {
      const { updateOnDuplicate } = options;

      if (Array.isArray(updateOnDuplicate) || typeof updateOnDuplicate === 'undefined') {
        return await Model.bulkCreate(values, { updateOnDuplicate, logging: true });
      } else {
        const data = [];

        // Assumes the 'updateOnDuplicate' property is an object
        const { keys, fields } = updateOnDuplicate;

        const where = values.reduce((a, b) => {
          if (!a[Op.or]) a[Op.or] = [];
          a[Op.or].push(filterSelectedProps(keys, b));

          return a;
        }, {});
        const findDups = await Model.findAll({ where });
        const payload = values.filter((e) => !findDups.find((dup) => keys.every((key) => dup[key] === e[key])));

        // Update duplicate keys
        for (let x = 0; x < findDups.length; x++) {
          const row = findDups[x];

          for (let y = 0; y < fields.length; y++) {
            const field = fields[y];
            row[field] = values.find((e) => keys.every((key) => e[key] === row.get(key)))?.[field] || row.get(field);
            await row.save();
          }
        }

        // Push result
        data.concat(findDups);

        // Insert Duplicate keys
        if (payload.length) {
          await Model.bulkCreate(payload);
        }

        // Merge inserted data to updated data
        return data.concat(payload);
      }
    }
  }
};

function filterSelectedProps(props, e) {
  return props.reduce((a, key) => (a = { ...a, [key]: e[key] }), {});
}
